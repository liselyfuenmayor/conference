<!-- 16/07/2018 Lisely Fuenmayor Vista para mostrar los blogs-->
@extends('layout')

@section('content')


    
    <!--Banner Inner-->
    <section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Últimas Publicaciones</h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i>Inicio</a></li>
                                    <li class="active">Blog</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->


    <main>
        <div class="lgx-page-wrapper">
            <!--News-->
            <section>
                <div class="container">
                    <div class="row">
                        @foreach ($posts as $post)
                        <div class="col-xs-12 col-sm-6">
                            <div class="lgx-single-news">
                                <center><img src="{{$post->ImageUrl }}" class="postimg"></center>
                                <div class="single-news-info">
                                    <div class="meta-wrapper">
                                        <span>{{ $post->published_at->diffForHumans() }}</span>
                                        <span> {{ $post->title }}</span>
                                        <span>{{ $post->category->name }}</span>
                                    </div>
                                    <h3 class="title"><a href="news-single.html">{{ $post->excerpt }}</a></h3>
                                    <a class="lgx-btn lgx-btn-white lgx-btn-sm" href="#"><span>Read More</span></a>
                                </div>
                            </div>
                        </div>
                       @endforeach  
                      
                       
                    </div>
                </div><!-- //.CONTAINER -->
            </section>
            <!--News END-->
        </div>
    </main>


@endsection 