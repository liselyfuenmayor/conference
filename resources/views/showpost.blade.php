<!-- 16/07/2018 Lisely Fuenmayor Vista para mostrar los blogs-->
@extends('layout')

@section('content')


    
    <!--Banner Inner-->
    <section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Brooklyn Beta was the most important conferen best tristique</h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                    <li><a href="news.html">News</a></li>
                                    <li class="active">Brooklyn Beta was the most important conferen best tristique</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->


    <main>
        <div class="lgx-post-wrapper">
            <!--News-->
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <article>
                                <header>
                                    <figure>
                                        <img src="assets/img/bannar5.jpg" alt="New"/>
                                    </figure>
                                    <div class="text-area">
                                        <div class="hits-area">
                                            <div class="date">
                                                <a href="#"><i class="fa fa-user"></i> Jonathon Roy</a>
                                                <a href="#"><i class="fa fa-calendar"></i> 12 January, 2017</a>
                                                <a href="#"><i class="fa fa-folder"></i> News</a>
                                                <a href="#"><i class="fa fa-comment"></i> 0 Comments</a>
                                                <a href="#"><i class="fa fa-heart"></i> Hits: 353</a>
                                            </div>
                                        </div>
                                        <h1 class="title">Brooklyn Beta was the most important conferen best tristique</h1>
                                    </div>
                                </header>
                                <section>
                                    <p>
                                        Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate tellus, sed laoreet est. Nam eget
                                        dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi imperdiet egestas ullamcorperet.
                                        Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse hendrerit et sapien ut pretium. Aenean
                                        nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum
                                        tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis diam metus, id commodo ex placerat
                                        vitae. Curabitur lobortis justo ante, in varius leo congue id. Etiam a libero elementum, posuere est at, sodales
                                        justo. Proin nec venenatis metus. Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate
                                        tellus, sed laoreet est. Nam eget dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi
                                        imperdiet egestas ullamcorperet. Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse
                                        hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam
                                        feugiat, pharetra arcu eget, bibendum tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis
                                        diam metus, id commodo ex placerat vitae.
                                    </p>
                                    <blockquote>
                                        <p>Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor. </p>
                                        <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                                    </blockquote>
                                    <blockquote>
                                        Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor.
                                    </blockquote>
                                    <h3>Unordered List Style</h3>
                                    <ul>
                                        <li>Reget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Faucibus viverra eget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Etiam nec urna in odio faucibus viverra eget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Lectus Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>nteger ac nunc sed lectus vehicula mollis. Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>Reget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Faucibus viverra eget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>nteger ac nunc sed lectus vehicula mollis. Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit efficitur vestibulum</li>
                                    </ul>
                                    <h3>Ordered List Style</h3>
                                    <ol>
                                        <li>Reget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Faucibus viverra eget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Etiam nec urna in odio faucibus viverra eget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Lectus Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>nteger ac nunc sed lectus vehicula mollis. Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>Reget nec nunc. Etiam posuere iaculis quam.</li>
                                        <li>Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit efficitur vestibulum</li>
                                        <li>nteger ac nunc sed lectus vehicula mollis. Ut in ante et quam malesuada gravida. Morbi tempus, odio suscipit.</li>
                                    </ol>
                                    <h3>Table Style Here</h3>
                                    <table class="table lgx-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Column Heading</th>
                                            <th>Column Heading</th>
                                            <th>Column Heading</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3>Images Alignment</h3>
                                    <p>
                                        <img class="lgx-img-left" src="assets/img/image-alignment.jpg" alt="image"/>
                                        Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate tellus, sed laoreet est. Nam eget dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi imperdiet egestas ullamcorperet. Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis diam metus, id commodo ex placerat vitae. Curabitur lobortis justo ante, in varius leo congue id. Etiam a libero elementum, posuere est at, sodales justo. Proin nec venenatis metus. Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate tellus, sed laoreet est. Nam eget dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi imperdiet egestas ullamcorperet. Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis diam metus, id commodo ex placerat vitae. Curabitur lobortis justo ante.
                                    </p>
                                    <p>
                                        <img class="lgx-img-right" src="assets/img/image-alignment.jpg" alt="image"/>
                                        Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate tellus, sed laoreet est. Nam eget dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi imperdiet egestas ullamcorperet. Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis diam metus, id commodo ex placerat vitae. Curabitur lobortis justo ante, in varius leo congue id. Etiam a libero elementum, posuere est at, sodales justo. Proin nec venenatis metus. Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate tellus, sed laoreet est. Nam eget dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi imperdiet egestas ullamcorperet. Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis diam metus, id commodo ex placerat vitae.
                                    </p>
                                    <img class="lgx-img-center" src="assets/img/image-alignment.jpg" alt="image"/>
                                    <p>
                                        Cras porttitor convallis ligula, at elementum erat mattis quis. In vitae vulputate tellus, sed laoreet est. Nam eget dolor non eros rutrum facilisis ut vel sapien. Aenean sed vehicula massa. Morbi imperdiet egestas ullamcorperet. Expo neque, congue nec nibh in, pellentesque fringilla risus. Suspendisse hendrerit et sapien ut pretium. Aenean nulla ipsum, facilisis eu porta non, rutrum sit amet nibh. Quisque id diam feugiat, pharetra arcu eget, bibendum tortor. Vivamus aliquam lacus id leo tristique sagittis. Pellentesque mattis diam metus, id commodo ex placerat vitae. Curabitur lobortis justo ante, in varius leo congue id. Etiam a libero elementum, posuere est at, sodales justo. Proin nec venenatis metus.
                                    </p>
                                </section>
                                <footer>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="title">Share</h4>
                                            <div class="lgx-share">
                                                <ul class="list-inline lgx-social">
                                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            </article>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </section>
            <!--News END-->
        </div>
    </main>


@endsection 