<!-- 14/07/2018 Lisely Fuenmayor Layout Para el Landing Page-->
<!-- Se agrego la estructura del navbar y el footer a un layout, en caso de querer utilizarlo en otras vistas -->

<!doctype html>
<html class="no-js" lang="es">

<!-- Mirrored from themearth.com/demo/html/emeet/view/index12.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Jul 2018 01:51:26 GMT -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>BlockChain</title>
    <meta name="description" content="Responsive EventHunt HTML Template"/>
    <meta name="keywords" content="Bootstrap3, Event,  Conference, Meetup, Template, Responsive, HTML5"/>
    <meta name="author" content="themearth.com"/>
    

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@yourtwitterusername"/>
    <meta name="twitter:creator" content="@yourtwitterusername"/>
    <meta name="twitter:url" content="http://yourdomain.com/"/>
    <meta name="twitter:title" content="Your home page title, max 140 char"/>
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char "/>
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="assets/img/twittercardimg/twittercard-280-150.jpg"/>
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Your home page title"/>
    <meta property="og:url" content="http://your domain here.com"/>
    <meta property="og:locale" content="es_ES"/>
    <meta property="og:site_name" content="Your site name here"/>
    <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/img/opengraph/fbphoto.jpg"/>
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->
                                                    
    <!--  FAVICON AND TOUCH ICONS -->
<link rel="shortcut icon" type="image/x-icon" href=" {{ asset('img/favicon.ico') }} "/>
    <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('img/favicon/manifest.json') }}">

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{ asset('libs/fontawesome/css/font-awesome.min.css') }}" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{ asset('libs/maginificpopup/magnific-popup.css') }}" media="all"/>

    <!-- Time Circle -->
    <link rel="stylesheet" href="{{ asset('libs/timer/TimeCircles.css') }}" media="all"/>

    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="{{ asset('libs/owlcarousel/owl.carousel.min.css') }}" media="all" />
    <link rel="stylesheet" href="{{ asset('libs/owlcarousel/owl.theme.default.min.css') }}" media="all" />

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oswald:400,700%7cRaleway:300,400,400i,500,600,700,900"/>

    <!-- MASTER  STYLESHEET  -->
    <link id="lgx-master-style" rel="stylesheet" href="{{ asset('css/style-default.min.css') }}" media="all"/>

      <link id="lgx-master-style" rel="stylesheet" href="{{ asset('css/estilos.css') }}" media="all"/>

    <!-- MODERNIZER CSS  -->
    <script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>

    
    </head>

<body class="home">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="lgx-container ">
<!-- ***  ADD YOUR SITE CONTENT HERE *** -->


<!--HEADER-->
<header>
    <div id="lgx-header" class="lgx-header">
        <div class="lgx-header-position"> <!--lgx-header-position-fixed lgx-header-position-white lgx-header-fixed-container lgx-header-fixed-container-gap lgx-header-position-white-->
            <div class="lgx-container-fluid"> <!--lgx-container-fluid-->
                <nav class="navbar navbar-default lgx-navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="lgx-logo">
                            <a href="{{ url('/') }}" class="lgx-scroll">
                                <img src="{{asset('img/logo.png')}}" alt="Eventhunt Logo"/>
                            </a>
                        </div>
                    </div>
                  <div id="navbar" class="navbar-collapse collapse lgx-collapse">
                        <ul class="nav navbar-nav lgx-nav">
                            <li><a class="lgx-scroll" href="#lgx-schedule">Eventos</a></li>
                            <li><a class="lgx-scroll" href="#lgx-speakers">Expositores</a></li>                    
                            <li><a class="lgx-scroll" href="#lgx-sponsors">Sponsors</a></li>
                            <li><a class="lgx-scroll" href="#lgx-news">Publicaciones</a></li>
                          
                        </ul>
                      
                    </div><!--/.nav-collapse -->
                
                </nav>
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</header>
<!--HEADER END-->


@yield('content')




   <footer>
        <div id="lgx-footer" class="lgx-footer lgx-footer-black"> <!--lgx-footer-white-->
            <div class="lgx-inner-footer">
                <div class="lgx-subscriber-area"> <!--lgx-subscriber-area-indiv-->
                    <div class="container">
                        <div class="lgx-subscriber-inner">  <!--lgx-subscriber-inner-indiv-->
                            <h3 class="subscriber-title">Recibe Noticias de nosotros</h3>
                            <form class="lgx-subscribe-form" >
                                <div class="form-group form-group-email">
                                    <input type="email" id="subscribe" placeholder="Escribe tu dirección de correo" class="form-control lgx-input-form form-control"  />
                                </div>
                                <div class="form-group form-group-submit">
                                    <button type="submit" name="lgx-submit" id="lgx-submit" class="lgx-btn lgx-submit"><span>Suscribete</span></button>
                                </div>
                            </form> <!--//.SUBSCRIBE-->
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="lgx-footer-area lgx-footer-area-center">
                        <div class="lgx-footer-single">
                            <h3 class="footer-title">Redes Sociales</h3>
                            <p class="text">
                               Conectate con nosotros <br> por cualquiera de estas redes
                            </p>
                            <ul class="list-inline lgx-social-footer">
                                <li><a href="https://www.facebook.com/ExpoCB2018" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/expocb2018" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/ExpoCB2018" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/ExpoCB2018" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="lgx-footer-single">

                            <h3 class="footer-title">Quorum Quito, Paseo San Francisco, Cumbayá </h3>

                            

                            <h4 class="date">
                                17 de noviembre del 2018
                            </h4>
                            <address>

                                Urb. Santa Lucía, pasaje A y Vía Interoceánica, <br>Paseo San Francisco, Cumbayá
                                <br> Quito - Ecuador

                                
                            </address>
                            
                        </div>
                        <div class="lgx-footer-single">
                            {{-- <h2 class="footer-title">Instagram Feed</h2>
                            <div id="instafeed"> --}}
                                <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                                <div class="elfsight-app-87447f17-af92-493a-96c7-eea9d483d6f0"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal-->
                    <div id="lgx-modal-map" class="modal fade lgx-modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="lgxmapcanvas map-canvas-default" id="map_canvas"> </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- //.Modal-->

                    <div class="lgx-footer-bottom">
                        <div class="lgx-copyright">
                            <p> <span>©</span> 2018 ExpoCryptoBlockchain Desarrollado por <a href="http://www.smartpcecuador.com/">SmartpcEcuador.</a> Todos los derechos reservados.</p>
                        </div>
                    </div>

                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.footer Middle -->
        </div>
    </footer>
    <!--FOOTER END-->

</div>
<!--//.LGX SITE CONTAINER-->
<!-- *** ADD YOUR SITE SCRIPT HERE *** -->
<!-- JQUERY  -->
<script src="{{ asset('js/vendor/jquery-1.12.4.min.js') }}"></script>

<!-- BOOTSTRAP JS  -->
<script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- Smooth Scroll  -->
<script src="{{ asset('libs/jquery.smooth-scroll.js') }}"></script>

<!-- SKILLS SCRIPT  -->
<script src="{{ asset('libs/jquery.validate.js') }}"></script>

<!-- if load google maps then load this api, change api key as it may expire for limit cross as this is provided with any theme -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQvRGGtL6OrpP5xVMxq_0NgiMiRhm3ycI"></script>

<!-- CUSTOM GOOGLE MAP -->
<script type="text/javascript" src="{{ asset('libs/gmap/jquery.googlemap.js') }}"></script>

<!-- adding magnific popup js library -->
<script type="text/javascript" src="{{ asset('libs/maginificpopup/jquery.magnific-popup.min.js') }}"></script>

<!-- Owl Carousel  -->
<script src="{{ asset('libs/owlcarousel/owl.carousel.min.js') }}"></script>

<!-- COUNTDOWN   -->
<script src="{{ asset('libs/countdown.js') }}"></script>
<script src="{{ asset('libs/timer/TimeCircles.js') }}"></script>

<!-- Counter JS -->
<script src="{{ asset('libs/waypoints.min.js') }}"></script>
<script src="{{ asset('libs/counterup/jquery.counterup.min.js') }}"></script>

<!-- SMOTH SCROLL -->
<script src="{{ asset('libs/jquery.smooth-scroll.min.js') }}"></script>
<script src="{{ asset('libs/jquery.easing.min.js') }}"></script>

<!-- type js -->
<script src="{{ asset('libs/typed/typed.min.js') }}"></script>

<!-- header parallax js -->
<script src="{{ asset('libs/header-parallax.js') }}"></script>

<!-- instafeed js -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>-->
<script src="{{ asset('libs/instafeed.min.js') }}"></script>

<!-- CUSTOM SCRIPT  -->
<script src="{{ asset('js/custom.script.js') }}"></script>

@yield('scripts')

</body>

<!-- Mirrored from themearth.com/demo/html/emeet/view/index12.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Jul 2018 01:51:26 GMT -->
</html>
