<!--14/07/2018 Lisely Fuenmayor, En esta vista se encuentra el contenido del landing Page-->

@extends('layout') @section('content')
  <!--BANNER-->
<section>
    <div class="lgx-banner lgx-banner16">
        <div class="lgx-banner-style">
            <div class="lgx-inner lgx-inner-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-banner-info"> <!--lgx-banner-info-center lgx-banner-info-black lgx-banner-info-big lgx-banner-info-bg--> <!--banner-info-margin-->
                                <h3 class="subtitle">Conferencia</h3>
                                <h2 class="title">ExpocryptoBlockchain 2018</h2>
                                <h3 class="location"><i class="fa fa-map-marker"></i> Quorum Quito, Paseo San Francisco, Cumbayá</h3>
                                <div class="action-area">
                                    <div class="lgx-video-area">
                                        <p class="video-area"><a id="myModalLabel" class="icon" href="#" data-toggle="modal" data-target="#lgx-modal">
                                            <i class="fa fa-play " aria-hidden="true"></i>
                                        </a> Watch Promo Video</p>
                                        <!-- Modal-->
                                        <div id="lgx-modal" class="modal fade lgx-modal">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <iframe id="modalvideo" src="https://www.youtube.com/embed/oSPR5Go05Vg" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- //.Modal-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </div>
</section>
<!--BANNER END-->
<!--countdown-->
<section>
    <div class="lgx-countdown">
        <div class="lgx-inner-countdown">
            <div class="countdown-left-info">
                <h2 class="title">Apresúrate</h2>
                <h3 class="subtitle">Has tu reservación hoy mismo.</h3>
                <p class="date">17 de noviembre del 2018</p>
            </div>
            <div class="countdown-right">
                <div class="lgx-countdown-area lgx-countdown-simple">
                    <!-- Date Format :"Y/m/d" || For Example: 1017/10/5  -->
                    <div id="lgx-countdown" data-date="2018/11/17"></div>
                </div>
            </div>
        </div><!-- //.INNER -->
    </div>
</section>
<!--countdown END-->
<!--VIDEO-->
<section>
    <div id="lgx-video" class="lgx-video">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!--<h2 class="lgx-video-title"><span>Watch Our Promo video!</span>How to make an online order</h2>-->
                        <div class="lgx-video-area">
                            <figure>
                                <figcaption>
                                    <div class="video-icon">
                                        <div class="lgx-vertical">
                                            <a id="myModalLabel" class="icon" href="#" data-toggle="modal" data-target="#lgx-modal">
                                                <i class="fa fa-play " aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <!-- Modal-->
                            <div id="lgx-modal" class="modal fade lgx-modal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <iframe id="modalvideo" src="https://www.youtube.com/embed/pFZQHsAdNds" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- //.Modal-->
                        </div>
                    </div>
                </div>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--//.VIDEO END-->
{{-- <section> <!--Esta es la seccion donde se encuentran los sponsors Gold-->
    <div id="lgx-sponsors" class="lgx-sponsors"">
        <div class="lgx-inner">
            <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="lgx-heading">
                                    <h2 class="heading">Patrocinadores Oficiales</h2>
                                </div>
                                <div class="sponsors-area">
                             <!-- Este for each recorre todos los resultados que devuelve la consulta -->
                                @foreach($sponsorsGold as $sponsorGold)
                                    <div class="single">
                                        <a class="" href="{{ route('sponsor', $sponsorGold->id) }}"><img height="230" width="172" src="{{ $sponsorGold->ImageUrl }}" alt="sponsor"/></a>
                                        <h3 class="title">{{ $sponsorGold->name }}</h3>
                                    </div>
                                @endforeach 
                                </div>
                            </div>
                            <!--//col-->
                        </div>
                        <!--//row-->
                    </div>
                </div>
            </div>
</section> --}}
<!--SCHEDULE-->
<section><!--Este es el calendario de eventos incluye la busqueda Events-->
    <div id="lgx-schedule" class="lgx-schedule lgx-schedule-white">
<div class="lgx-inner">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="lgx-heading">
                <h2 class="heading">Calendario de eventos</h2>
                {{-- <h3 class="subheading"> Agregar un subtitulo aquí</h3> --}}
            </div>
        </div>
    </div>
    <!-- Recorres los resultados obtenidos para los eventos  -->
    @foreach ($events as $event)
    <div class="row">
        <div class="col-xs-12">
        <div class="lgx-tab lgx-tab-vertical">
            <ul class="nav nav-pills lgx-nav lgx-nav-nogap lgx-nav-colorful">
                <!-- Estas son las pestañas de los eventos -->
                <li class=" tagfecha " id="{{ $event->id }}"><a data-toggle="pill" href="#home"><h3><span>{{ $event->EventDay }}</span></h3> <p>{{ $event->day }}</p></a></li> <!--Uso del atributo calculado EventDay-->
            </ul>
            <div class="tab-content lgx-tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default lgx-panel">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <div class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <div class="lgx-single-schedule">
                                            <div class="author">
                                                <img src="{{ $event->speaker['ImageUrl'] }}" alt="Speaker"/>
                                            </div>
                                            <div class="schedule-info">
                                                <h4 class="time">Hora: {{ $event->hour }}</h4>
                                                <h3 class="title">{{ $event->name }}</h3>
                                                <h4 class="author-info">Por <span>{{ $event->speaker['name'] }}</span></h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p class="text">
                                       {{$event->description}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
               </div>
            </div>
            </div>
    @endforeach

    <!--//.ROW-->
  <!--   <div class="section-btn-area schedule-btn-area">
        <a class="lgx-btn lgx-btn-big" href="speakers.html"><span>Download Schedule (PDF)</span></a>
        <a class="lgx-btn lgx-btn-red lgx-btn-big" href="speakers.html"><span>Connect via facebook</span></a>
    </div> -->
      </div>
    <!-- //.CONTAINER -->
   </div>
<!-- //.INNER -->
</div>
</section>
<!--SCHEDULE END-->
<!--SPEAKERS-->
<section>
<!--En esta area se imprimen los expositores -->
    <div id="lgx-speakers" class="lgx-speakers lgx-speakers2">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading lgx-heading-white">
                            <h2 class="heading">Expositores</h2>
                            {{-- <h3 class="subheading">Agregar un subtitulo aquí</h3> --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- Busqueda de expositores -->
                    @foreach ($speakers as $speaker)
                        <div class="lgx-col3 lgx-single-speaker2">
                            <figure>
                                <a class="profile-img" href="{{ $speaker->website }}" target="_blank">
                                <img height="500" width="400" src="{{ $speaker->ImageUrl }}" alt="speaker"/></a>
                                <figcaption>
                                    <div class="social-group">
                                        <a class="sp-tw" href="{{ $speaker->red1 }}" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a class="sp-fb" href="{{ $speaker->red2 }}" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a class="sp-insta" href="{{ $speaker->red3 }}" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                    <div class="speaker-info">
                                        <h3 class="title"><a >{{ $speaker->name }}</a></h3>
                                        <h4 class="subtitle">{{ $speaker->title }}</h4>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--SPEAKERS END-->
<!--SPONSORED-->
{{-- <section><!--Aquí van todos los sponsors Plata-->
    <div id="lgx-sponsors" class="lgx-sponsors">
        <div class="lgx-inner-bg">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading">
                                <h2 class="heading">Sponsors Gold</h2>
                            </div>
                        </div>
                    </div>
                 
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="sponsors-area">
                                    <!-- Busquesa que recorre los sponsors plata -->
                                @foreach ($sponsorsPlate as $sponsorplate)
                                    <div class="single">
                                        <a class="" href="#"><img  src="{{ $sponsorplate->ImageUrl }}" alt="sponsor"/></a>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <!--//col-->
                        </div>
                    <!--//row-->
                  <!--   <div class="section-btn-area sponsor-btn-area">
                        <a class="lgx-btn" href="#">Become A Sponsor</a>
                    </div> -->
                </div>
                <!--//container-->
            </div>
        </div>
        <!--//lgx-inner-->
    </div>
</section> --}}
<!--SPONSORED END-->

<section>




     <section>
        <div id="lgx-news" class="lgx-news">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading">
                                <h2 class="heading">Publicaciones</h2>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
   <!-- Recibe la consulta y muestra los dos post que consiguio  -->
                @foreach ($posts as $post)
                        <div class="col-xs-12 col-sm-6">
                            <div class="lgx-single-news">
                            <center><img src="{{$post->ImageUrl }}" class="postimg"></center>
                                
                                <div class="single-news-info">
                                    <div class="meta-wrapper">
                                        <span>{{ $post->published_at }}</span>
                                        <span> {{ $post->title }}</span>
                                        <span>{{ $post->category->name }}</span>
                                    </div> 
                                    
                                    <h3 class="title">
                                   

                                    <a href="news-single.html"> &nbsp; &nbsp; {{ $post->excerpt }}</a></h3>

                                   <a class="lgx-btn lgx-btn-white lgx-btn-sm" href="{{ url('/blogs/$post->id') }}" ><span>Leer más</span></a>
                                </div>
                            </div>
                        </div>
                @endforeach         
                  
                    </div>
                    <div class="section-btn-area">
                        <a class="lgx-btn" href=" {{ url('/blogs') }}">Ver todos las Publicaciones</a>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> 
<!--PHOTO GALLERY-->
    <section>
        <div id="lgx-photo-gallery" class="lgx-gallery-popup lgx-photo-gallery-normal lgx-photo-gallery-black">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading lgx-heading-white">
                                <h2 class="heading">Galeria de Fotos</h2>
                                {{-- <h3 class="subheading">Welcome to the dedicated to building remarkable Sponsores!</h3> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-gallery-area">
                                <div  class="lgx-gallery-single">
                                    <figure>
                                        <img title="Memories One" src="{{asset('img/gallery/img001.jpg')}}" alt="Memories one"/>
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a title="Memories One" href="{{ asset('img/gallery/img001.jpg')}}">
                                                        <i class="fa fa-chain-broken" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div> <!--Single photo-->
                                <div  class="lgx-gallery-single">
                                    <figure>
                                        <img src="{{asset('img/gallery/img002.jpg')}}" alt="Memories Two" title="Memories Two" />
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a title="Memories Two" href="{{ asset('img/gallery/img002.jpg')}}">
                                                        <i class="fa fa-chain-broken" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>  <!--Single photo-->
                                <div  class="lgx-gallery-single">
                                    <figure>
                                        <img src="{{asset('img/gallery/img003.jpg')}}" alt="Memories Three" title="Memories Three" />
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a title="Memories Three" href="{{ asset('img/gallery/img003.jpg')}}">
                                                        <i class="fa fa-chain-broken" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div> <!--Single photo-->
                                <div  class="lgx-gallery-single">
                                    <figure>
                                        <img src="{{asset('img/gallery/img004.jpg')}}" alt="Memories Four" title="Memories Four" />
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a title="Memories Four" href="{{ asset('img/gallery/img004.jpg')}}">
                                                        <i class="fa fa-chain-broken" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div> <!--Single photo-->
                                <div class="lgx-gallery-single">
                                    <figure>
                                        <img src="{{asset('img/gallery/img005.jpg')}}" alt="Memories Five" title="Memories Five" />
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a title="Memories Five" href="{{ asset('img/gallery/img005.jpg')}}">
                                                        <i class="fa fa-chain-broken" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div> <!--Single photo-->
                                <div class="lgx-gallery-single">
                                    <figure>
                                        <img src="{{asset('img/gallery/img006.jpg')}}" alt="Memories Six" title="Memories Six" />
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a title="Memories Six" href="{{ asset('img/gallery/img006.jpg')}}">
                                                        <i class="fa fa-chain-broken" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div> <!--Single photo-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--PHOTO GALLERY END-->    

<!-- En esta area van los sponsors bronce, que son mas pequeños -->
    {{-- <section>
    <div id="lgx-sponsors" class="lgx-sponsors"">
        <div class="lgx-inner">
            <div class="container">
      <!--//main row-->
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="sponsored-heading first-heading">Sponsors Plata</h3>
                                <div class="sponsors-area">
                                @foreach ($sponsorsBronce as $sponsorbronce)
                                    <div class="single">
                                        <a class="" href="#"><img class="spnbr" height="200" width="100" src="{{ $sponsorbronce->ImageUrl }}" alt="sponsor"/></a>
                                    </div>
                                @endforeach 
                                </div>
                            </div>
                            <!--//col-->
                        </div>
                        <!--//row-->
                    </div>
                </div>
            </div>
</section> --}}
@endsection
@section('scripts')

<!-- Este scritp funciona para cambiar el hover de las pestañas de los eventos -->

<script type="text/javascript">
    
    jQuery(document).ready(function(){

         $(".tagfecha").hover(function(e){
            e.preventDefault();

                var id = $(this).attr("id");

               $('#'+id).addClass('active');

               });

          $(".tagfecha").mouseleave(function(e){
             e.preventDefault();
              var id = $(this).attr("id");
              $('#'+id).removeClass('active');

          });
      
        });

</script>

@endsection 