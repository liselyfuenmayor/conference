@extends('layout') @section('content')

	<section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Sponsor Oficial </h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i>Inicio</a></li>
                                    <li class="active">Sponsors</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->

    <!--travelinfo-->
<section>
	<br><br><br>
	<div class="col-xs-12 col-sm-6 col-md-4">
        <div class="lgx-single-speaker2 lgx-single-speaker3">
            <figure>
                <a class="profile-img" href=""><img src="{{ $sponsor->ImageUrl }}" alt="sponsor"/></a>
                <figcaption>
                    <div class="social-group">
                        <a class="sp-tw" href="{{ $sponsor->red1 }}" target="_blank"><i class="fa fa-twitter fa-3x"></i></a>
                            <a class="sp-fb" href="{{ $sponsor->red2 }}" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>
                            <a class="sp-insta" href="{{ $sponsor->red3 }}" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
                    </div>
                    
                </figcaption>
            </figure>
        </div>
    </div>
    <div id="lgx-travelinfo" class="lgx-travelinfo">
        <div class="lgx-inners">
            <div class="lgx-leftright-area">
                
                <div class="lgx-left-area lgx-leftright-info">
                    <div class="lgx-leftright-info-inner">
                        <h3 class="title">{{ $sponsor->name }}</h3>
                        <p class="info">{{ $sponsor->title }}</p>
                        <p>
                            {{ $sponsor->description }}
                        </p>
                        <a class="lgx-btn lgx-btn-red" href="{{ $sponsor->website }}" target="_blank">Visitar Página Web</a>
                        <div class="social-group" style="">
                            <a class="sp-tw" href="{{ $sponsor->red1 }}" target="_blank"><i class="fa fa-twitter fa-3x"></i></a>
                            <a class="sp-fb" href="{{ $sponsor->red2 }}" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>
                            <a class="sp-insta" href="{{ $sponsor->red3 }}" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
                    	</div>
                    </div>
                </div>
            </div>
            
            
        </div>
        <!-- //.INNER -->
    </div>
</section>
<!--travelinfo END-->


@endsection
@section('scripts')

<!-- Este scritp funciona para cambiar el hover de las pestañas de los eventos -->

<script type="text/javascript">
    
    jQuery(document).ready(function(){

         $(".tagfecha").hover(function(e){
            e.preventDefault();

                var id = $(this).attr("id");

               $('#'+id).addClass('active');

               });

          $(".tagfecha").mouseleave(function(e){
             e.preventDefault();
              var id = $(this).attr("id");
              $('#'+id).removeClass('active');

          });
      
        });

</script>

@endsection 