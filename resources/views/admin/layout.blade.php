<!DOCTYPE html>
<html lang="es">
  
<!-- Mirrored from themes-lab.com/make/admin/layout1/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Jul 2018 03:17:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <link rel="shortcut icon" href="/adminlte/assets/global/images/favicon.png" type="image/png">
    <title>Administrador</title>
    <link href="/adminlte/assets/global/css/style.css" rel="stylesheet">
    <link href="/adminlte/assets/global/css/theme.css" rel="stylesheet">
    <link href="/adminlte/assets/global/css/ui.css" rel="stylesheet">
    <link href="/adminlte/assets/admin/layout1/css/layout.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->
    <link href="/adminlte/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
    <link href="/adminlte/assets/global/plugins/maps-amcharts/ammap/ammap.css" rel="stylesheet">

<!-- @stack('styles') -->

     @yield('styles')

    <!-- END PAGE STYLE -->
    <script src="/adminlte/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- PORTAL CSS  -->
    <link id="lgx-master-style" rel="stylesheet" href="{{ asset('css/portal.css') }}" media="all"/>
  </head>
  <!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
  <!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
  <!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
  <!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
  <!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
  <!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
  <!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
  <!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
  <!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

  <!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
  <!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
  <!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
  <!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->
  
  <!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
  <!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
  <!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
  <!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
  <!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
  <!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
  <!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
  <!-- BEGIN BODY -->
  <body class="fixed-topbar fixed-sidebar theme-sdtl color-default dashboard">
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <section>
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar">
        <div class="logopanel">
          <h1>
            <a href="{{ route('dashboard') }}"></a>
          </h1>
        </div>
        <div class="sidebar-inner">
          <div class="sidebar-top">
           {{--  <form action="http://themes-lab.com/make/admin/layout1/search-result.html" method="post" class="searchform" id="search-results">
              <input type="text" class="form-control" name="keyword" placeholder="Search...">
            </form> --}}
            <div class="userlogged clearfix">
              <i class="icon icons-faces-users-01"></i>
              <div class="user-details">
                <h4>Mike Mayers</h4>
                {{-- <div class="dropdown user-login">
                  <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300">
                  <i class="online"></i><span>Available</span><i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><i class="busy"></i><span>Busy</span></a></li>
                    <li><a  href="#"><i class="turquoise"></i><span>Invisible</span></a></li>
                    <li><a href="#"><i class="away"></i><span>Away</span></a></li>
                  </ul>
                </div> --}}
              </div>
            </div>  
          </div>
          <div class="menu-title">
           <strong> Menú Principal </strong>
          </div>
          @include('admin.partials.nav')
          <!-- SIDEBAR WIDGET FOLDERS -->
          {{-- <div class="sidebar-widgets">
            <p class="menu-title widget-title">Folders <span class="pull-right"><a href="#" class="new-folder"> <i class="icon-plus"></i></a></span></p>
            <ul class="folders">
              <li>
                <a href="#"><i class="icon-doc c-primary"></i>My documents</a> 
              </li>
              <li>
                <a href="#"><i class="icon-picture"></i>My images</a> 
              </li>
              <li><a href="#"><i class="icon-lock"></i>Secure data</a> 
              </li>
              <li class="add-folder">
                <input type="text" placeholder="Folder's name..." class="form-control input-sm">
              </li>
            </ul>
          </div> --}}
          
        </div>
      </div>
      <!-- END SIDEBAR -->
      <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
          <div class="header-left">
            <div class="topnav">
              <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
              
            </div>
          </div>
          <div class="header-right">
            <ul class="header-menu nav navbar-nav">
                           
              <!-- BEGIN USER DROPDOWN -->
              <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img src="/adminlte/assets/global/images/avatars/user1.png" alt="user image">
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#"><i class="icon-user"></i><span>My Profile</span></a>
                  </li>
                  
                  <li>
                    <a href="#"><i class="icon-logout"></i><span>Logout</span></a>
                  </li>
                </ul>
              </li>
              <!-- END USER DROPDOWN -->
              <!-- CHAT BAR ICON -->
              {{-- <li id="quickview-toggle"><a href="#"><i class="icon-bubbles"></i></a></li> --}}
            </ul>
          </div>
          <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <div class="page-content page-thin">
             @yield('header')

             @if (session()->has('flash'))
                <div class="alert alert-success media fade in"> {{ session('flash')}}</div>
             @endif
            @yield('content')
          <div class="footer">
            <div class="copyright">
              <p class="pull-left sm-pull-reset">
                <span>Copyright <span class="copyright">©</span> 2016 </span>
                <span>THEMES LAB</span>.
                <span>All rights reserved. </span>
              </p>
              <p class="pull-right sm-pull-reset">
                <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
              </p>
            </div>
          </div>
        </div>
        <!-- END PAGE CONTENT -->
      </div>
      <!-- END MAIN CONTENT -->
      
    <!-- BEGIN PRELOADER -->
    <div class="loader-overlay">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <!-- END PRELOADER -->
    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 
    <script src="/adminlte/assets/global/plugins/jquery/jquery-3.1.0.min.js"></script>
    <script src="/adminlte/assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
    <script src="/adminlte/assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="/adminlte/assets/global/plugins/gsap/main-gsap.min.js"></script>
    <script src="/adminlte/assets/global/plugins/tether/js/tether.min.js"></script>
    <script src="/adminlte/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/adminlte/assets/global/plugins/appear/jquery.appear.js"></script>
    <script src="/adminlte/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
    <script src="/adminlte/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
    <script src="/adminlte/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
    <script src="/adminlte/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
    <script src="/adminlte/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
    <script src="/adminlte/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
    <script src="/adminlte/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
    <script src="/adminlte/assets/global/plugins/select2/dist/js/select2.full.min.js"></script> <!-- Select Inputs -->
    <script src="/adminlte/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
    <script src="/adminlte/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
    <script src="/adminlte/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
    <script src="/adminlte/assets/global/js/builder.js"></script> <!-- Theme Builder -->
    <script src="/adminlte/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
    <script src="/adminlte/assets/global/js/application.js"></script> <!-- Main Application Script -->
    <script src="/adminlte/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
    <script src="/adminlte/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
    <script src="/adminlte/assets/global/js/quickview.js"></script> <!-- Chat Script -->
    <script src="/adminlte/assets/global/js/pages/search.js"></script> <!-- Search Script -->
    <!-- BEGIN PAGE SCRIPT -->
    <script src="/adminlte/assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
    <script src="/adminlte/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications -->
    <script src="/adminlte/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> <!-- Inline Edition X-editable -->
    <script src="/adminlte/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> <!-- Context Menu -->
    <script src="/adminlte/assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> <!-- Multi dates Picker -->
    <script src="/adminlte/assets/global/plugins/charts-chartjs/Chart.min.js"></script>  <!-- ChartJS Chart -->
    <script src="/adminlte/assets/global/plugins/charts-highstock/js/highstock.js"></script> <!-- financial Charts -->
    <script src="/adminlte/assets/global/plugins/charts-highstock/js/modules/exporting.js"></script> <!-- Financial Charts Export Tool -->
    <script src="/adminlte/assets/global/plugins/maps-amcharts/ammap/ammap.js"></script> <!-- Vector Map -->
    <script src="/adminlte/assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.js"></script> <!-- Vector World Map  -->
    <script src="/adminlte/assets/global/plugins/maps-amcharts/ammap/themes/black.js"></script> <!-- Vector Map Black Theme -->
    <script src="/adminlte/assets/global/plugins/skycons/skycons.min.js"></script> <!-- Animated Weather Icons -->
    <script src="/adminlte/assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script> <!-- Weather Plugin -->
    <script src="/adminlte/assets/global/js/widgets/todo_list.js"></script>
    <script src="/adminlte/assets/global/js/widgets/widget_weather.js"></script>
    <script src="/adminlte/assets/global/js/pages/dashboard.js"></script>

     <!-- @stack('scripts') -->

     @yield('scripts')

    


    <!-- END PAGE SCRIPT -->
    <script src="/adminlte/assets/admin/layout1/js/layout.js"></script>
  </body>

<!-- Mirrored from themes-lab.com/make/admin/layout1/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Jul 2018 03:18:41 GMT -->
</html>

