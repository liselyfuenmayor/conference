@extends('admin.layout')

@section('content')
	<h1>Usuarios</h1>
	<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table.responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>id</th>
						<th>Nombre de Usuario</th>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Correo</th>
						<th>Dirección</th>
						<th>Telefono</th>
						<th>Rol</th>
						<th>Opciones</th>
					</thead>
					@foreach ($users as $user)
					<tr>
						<td>{{$user->id}}</td>
						<td>{{$user->username}}</td>
						<td>{{$user->name}}</td>
						<td>{{$user->lastname}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->address}}</td>
						<td>{{$user->phone}}</td>
						<td>{{$user->nombre_rol}}</td>
						<td>
							<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#editUserModal" onclick="findUserById({{$user->id}})"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
							<button class="btn btn-danger btn-xs" onclick="deleteUser({{$user->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
						</td>
					</tr>
					@endforeach
			</table>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
      </div>
      <div class="modal-body">
        <section>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="username">Nombre Usuario</label>
						<input type="text" class="form-control" id="username" name="username" placeholder="Nombre Usuario">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="name">Nombres</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Nombres">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="lastname">Apellidos</label>
						<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Apellidos">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="name">Correo</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Correo">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="address">Dirección</label>
						<input type="text" class="form-control" id="address" name="address" placeholder="Dirección">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="phone">Telefono</label>
						<input type="number" class="form-control" id="phone" name="phone" placeholder="Telefono">
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="rol_id">Roles</label>						
						<select id="id_rol" name="id_rol" placeholder="Roles">
						@foreach ($roles as $rol)
							<option value="{{$rol->id}}">{{$rol->name}}</option>
						@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4">
					
				</div>
				<div class="col-md-4">

				</div>
			</div>
		</section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrrar</button>
        <button type="button" class="btn btn-primary" onclick="editUser()">Editar</button>
      </div>
    </div>
  </div>
</div>

<script>
	  var idUserEdit;
	  
      function findUserById(idUser){
		idUserEdit=idUser;
		$.ajax({
			type: "POST",
			url: "{{ URL::route('findUserById') }}",
			data: {
				"_token": "{{ csrf_token() }}",
				'id_user': idUser
				},
			success: function(data){
						$('#username').val(data.username);
						$('#name').val(data.name);
						$('#lastname').val(data.lastname);
						$('#email').val(data.email);
						$('#address').val(data.address);
						$('#phone').val(data.phone);
						var selectRoles = $('#id_rol');
						selectRoles=selectRoles[0];
						var optionRoles = $(selectRoles).children('option');
						for(var i=0; i<optionRoles.length; i++){
							if($(optionRoles[i]).val()==data.role_id){
								console.log("son iguales");
								console.log(optionRoles[i]);
								$(optionRoles[i]).attr('selected', true);
							}
						}												
					 }
				});
		    }

		    function editUser(){
				console.log('edit user');
				var username = $('#username').val();
				var name = $('#name').val();
				var lastname = $('#lastname').val();
				var email = $('#email').val();
				var address = $('#address').val();
				var phone = $('#phone').val();
				var id_rol = $('#id_rol').val();
				console.log(id_rol);
				$.ajax({
			type: "POST",
			url: "{{ URL::route('editUser') }}",
			data: {
				"_token": "{{ csrf_token() }}",
				'id_user': idUserEdit,
				'username' : username,
				'name' : name,
				'lastname' : lastname,
				'email' : email,
				'address' : address,
				'phone' : phone,
				'role_id' : id_rol
				},
			success: function(data){
				alert('Usuario Editado');
                      window.location.pathname='/user';						
					 }
				});
			}

			function deleteUser(idUser){
				if(confirm("¿Está seguro de eliminar el usuario?")){
					$.ajax({
					type: "POST",
					url: "{{ URL::route('deleteUser') }}",
					data: {
						"_token": "{{ csrf_token() }}",
						'id_user': idUser
						},
						success: function(data){
							alert('Usuario Eliminado');
								window.location.pathname='/user';						
								}
							});
						}
				}
			



</script>

@endsection
