@extends('admin.layout')

@section('content')
	
<div>
	
	<div  class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label >Nombre de Usuario</label>
			<input type="text" class="form-control" name="username" placeholder="Nombre de Usuario">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label >Nombres</label>
			<input type="text" class="form-control" name="name" placeholder="Nombres">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label >Apellidos</label>
			<input type="text" class="form-control" name="lastname" placeholder="Apellidos">
		</div>
	</div>
	</div>
	<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label >Email</label>
			<input type="text" class="form-control" name="email" placeholder="Email">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label >Direccion</label>
			<input type="text" class="form-control" name="address" placeholder="Direccion">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label >Telefono</label>
			<input type="text" class="form-control" name="phone" placeholder="Telefono">
		</div>
	</div>
	</div>
	<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label >Password</label>
			<input type="text" class="form-control" name="password" placeholder="Password">
		</div>
	</div>
	<div class="col-md-4">
		<select class="form-control" name="role_id">
		@foreach ($roles as $rol)
			<option value="{{$rol->id}}">{{$rol->name}}</option>
		@endforeach
		</select>
	</div>
	<div class="col-md-4">
		<label ></label>
		<button type="button" class="btn btn-primary" onclick="crearUsuario()">Crear Usuario</button>
	</div>
	</div>

</div>


<script>
      function crearUsuario(){
          console.log('username '+$('input[name=username]').val());
          console.log('name '+$('input[name=name]').val());
          console.log('lastname '+$('input[name=lastname]').val());
          console.log('email '+$('input[name=email]').val());
          console.log('address '+$('input[name=address]').val());
          console.log('phone '+$('input[name=phone]').val());
          console.log('password '+$('input[name=password]').val());


          var username = $('input[name=username]').val();
          var name = $('input[name=name]').val();
          var lastname = $('input[name=lastname]').val();
          var email = $('input[name=email]').val();
          var address = $('input[name=address]').val();
          var phone = $('input[name=phone]').val();
          var password = $('input[name=password]').val();
          var role_id = $('select[name=role_id]').val();

            $.ajax({
                type: 'POST',
                url: "{{ URL::route('storeUsuario') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    'username': username,
                    'name': name,
                    'lastname': lastname,
                    'address': address,
                    'phone': phone,
                    'password': password,
					'email': email,
					'role_id':role_id

                },
                success: function(data) {


                    alert('Usuario Creado Exitosamente');
                    window.location.pathname='/user';

                },
            });
           
        }
</script>

@endsection