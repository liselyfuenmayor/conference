<!-- 15/07/2018 Lisely fuenmayor: Vista para crear los eventos-->
@extends('admin.layout')

@section('styles')

 <!-- <link href="{{ asset('adminlte/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
<link href="{{ asset('adminlte/assets/global/plugins/rateit/rateit.css') }}" rel="stylesheet"> -->

<link href="{{ asset('adminlte/assets/global/plugins/Jquery-validity/css/jquery.validity.css') }}" rel="stylesheet">

@endsection 

@section('content')
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="header">
    <h2>Crea un nuevo <strong>Evento</strong></h2>
    <!--<div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li><a href="#">Make</a>
        </li>
        <li><a href="forms.html">Forms</a>
        </li>
        <li class="active">Input Masks</li>
      </ol>
    </div>-->
  </div>
  <div class="row">
    <div class="col-lg-12 portlets">
      <!--<div class="panel"> -->
      <form class="panel" id="form" method="post" action="{{ route('admin.events.store')}}" enctype="multipart/form-data">
      	{{ csrf_field() }}
        <!--<div class="panel-header panel-controls">
          <h3><i class="icon-bulb"></i> Input <strong>Masks</strong></h3>
        </div>-->
        <div class="panel-content">
        @if (session('notification'))
            <div class="alert alert-success">
                <p>{{ session('notification') }}</p>
            </div>
        @endif
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" placeholder="Ingresa el nombre" name="name" id="Nombre" value="{{ old('name') }}"  >
              </div>

               <div class="form-group">
                        <label class="form-label">Día</label>
                        <div class="prepend-icon">
                          <input type="text" name="dia" id="datepicker" class="date-picker form-control" placeholder="Seleccione una fecha" value="{{ old('dia') }}" >
                          <i class="icon-calendar"></i>
                        </div>
              </div>

             <!--  <div class="col-md-6">
                          <div class="form-group">
                            <label class="form-label">Hora</label>
                            <div class="prepend-icon">
                              <input type="text" name="timepicker" class="timepicker form-control" placeholder="Selecciona la hora">
                              <i class="icon-clock"></i>
                            </div>
                          </div>
              </div> -->

            

               <div class="form-group">
                <label class="form-label">Hora</label>
                <input type="time" class="form-control" placeholder="ingrese la hora"  name="hora" id="Hora" value="{{ old('hora') }}" >
              </div>
           
           
            </div>
            <div class="col-md-6">

              <div class="form-group">
                <label class="form-label">Descripción</label>
                <textarea name="description" id="Descripcion" rows="7" class="form-control" placeholder="Escriba la descripción del expositor... (maximo 170 caracteres)" maxlength="170">{{ old('description') }}</textarea>
              </div>

            <div class="form-group">
                    <label><strong>Expositor</strong></label>
                  <select name="expositor" id="expositor" class="form-control" data-placeholder="Seleccione un expositor" >
                         <option value="">Seleccione la clase</option>
                         @foreach ($speakers as $speaker)
                            <option value="{{$speaker->id}}">{{$speaker->name}}</option>
                          
                         @endforeach
                  </select>
                    
            </div>
           

            
             
            </div>
          </div>
              <button  class="btn btn-primary" type="submit">Registrar Evento</button>
              <a  class="btn btn-default" href="{{ url('admin') }}">Volver atrás</a>
        </div>

      <!--</div>-->
      </form>
    </div>
  </div>

</div>
<!-- END PAGE CONTENT -->

@endsection


@section('scripts')

<!-- <script src="{{ asset('adminlte/assets/global/plugins/timepicker/jquery-ui-timepicker-addon.min.js')}}"> </script> 

<script src="{{ asset('adminlte/assets/global/plugins/multidatepicker/multidatespicker.min.js') }} "></script> 

<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script> 
 -->

<script type="text/javascript" src="{{ asset('adminlte/assets/global/plugins/Jquery-validity/js/jquery.validity.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminlte/assets/global/js/jquery.validations.js') }}"></script>


<script type="text/javascript">
 var today = new Date();
 $('#datepicker').datepicker({
   
    minDate: today 
  
 });

 </script>

 @endsection
