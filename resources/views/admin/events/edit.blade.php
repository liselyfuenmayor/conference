<!-- 15/07/2018 Lisely fuenmayor: Vista para editar los eventos-->
@extends('admin.layout')


@section('styles')

<link href="{{ asset('adminlte/assets/global/plugins/Jquery-validity/css/jquery.validity.css') }}" rel="stylesheet">

@endsection 

@section('content')
<div class="header">
        <h2>Editar datos de <strong>{{ $events->name }}</strong></h2>
  </div>
	<form class="panel" id="form" method="post" action="{{ route('admin.events.update') }}" enctype="multipart/form-data" >

   @if (session('notification'))
            <div class="alert alert-success">
                <p>{{ session('notification') }}</p>
            </div>
        @endif
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

  {{ csrf_field() }}
  <div class="panel-content">
          <div class="row">
            <div class="col-md-6">
             
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" name="name" id="Nombre"  value="{{ $events->name }}">
              </div>

              
                <div class="form-group">
                        <label class="form-label">Día</label>
                        <div class="prepend-icon">
                          <input type="text" name="dia" id="datepicker" class="date-picker form-control" placeholder="{{ $events->day }}" value="{{ $events->day }}" >
                          <i class="icon-calendar" ></i>
                        </div>
                </div>


               <div class="form-group">
                <label class="form-label">Hora</label>
                <input type="time" class="form-control"  name="hora" id="Hora" value="{{ $events->hour }}">
              </div>
           
            </div>
            <div class="col-md-6">

               <div class="form-group">
                  <label class="form-label">Descripción</label>
                  <textarea name="description" id="Descripcion" rows="7" class="form-control" placeholder="Escriba la descripción del expositor... (maximo 170 caracteres)" maxlength="170">{{$events->description}}</textarea>
              </div>

                <div class="form-group">
                        <label><strong>Expositor</strong></label>
                      <select name="expositor" id="expositor" class="form-control" data-placeholder="Seleccione un expositor" >
                             <option value="">Seleccione la clase</option>
                              <option value="{{$events->idspeaker}}" selected > {{ $events->speaker['name'] }}</option>
                             @foreach ($speakers as $speaker)
                                   @if($events->idspeaker != $speaker->id)

                                      <option value="{{$speaker->id}}">{{$speaker->name}}</option>
                                   @endif
                             @endforeach
                      </select>
                        
                </div>
           
              
             



            
            
            </div>
          </div>

      <input type="hidden" name="id" value="{{ $events->id }}">
      <button class="btn btn-primary" type="submit">Actualizar Datos</button>
      <a  class="btn btn-default" href="{{ url('admin/events') }}">Volver atrás</a>
      </div>
    </form>

 
@endsection

@section('scripts')


<script type="text/javascript" src="{{ asset('adminlte/assets/global/plugins/Jquery-validity/js/jquery.validity.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminlte/assets/global/js/jquery.validations.js') }}"></script>

<!-- Validaciones del front end -->


<script type="text/javascript">
 var today = new Date();
 $('#datepicker').datepicker({
   
    minDate: today 
  
 });

 </script>

 @endsection