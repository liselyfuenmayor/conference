<!-- 15/07/2018 Lisely fuenmayor: Vista para ver  los datos de los eventos-->
@extends('admin.layout')

@section('styles')
    <link href="{{ asset('adminlte/assets/global/plugins/magnific/magnific-popup.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/assets/global/plugins/hover-effects/hover-effects.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/assets/global/css/estilos.css') }}" rel="stylesheet">

@endsection


@section('content')

	
		<div class="header">
		    <h2>Datos de <strong>{{ $events->name }}</strong></h2>
	    </div>
      <div class="panel">
       <div class="panel-content">
          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" name="name" readonly value="{{ $events->name }}">
              </div>

               <div class="form-group">
                <label class="form-label">Día</label>
                <input type="date" class="form-control" name="dia" readonly value="{{ $events->day }}">
              </div>

               <div class="form-group">
                <label class="form-label">Hora</label>
                <input type="time" class="form-control"  name="hora" readonly value="{{ $events->hour }}">
              </div>
           
            </div>
            <div class="col-md-6">

               <div class="form-group">
                  <label class="form-label">Descripción</label>
                  <textarea name="description" id="Descripcion" rows="7" class="form-control" readonly >{{$events->description}}</textarea>
              </div>

              
 
             <div class="form-group">
                    <label class="form-label">Expositor</label>
                    <input type="text" name="expositor" class="form-control" readonly value="{{ $events->speaker['name'] }}">
              </div>
             
            </div>
          </div>
      <a  class="btn btn-default" href="{{ url('admin/events') }}">Volver atrás</a>
     
    </div>
      </div>

       
@endsection
