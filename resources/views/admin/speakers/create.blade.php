<!-- 14/07/2018 Lisely fuenmayor: Vista para crear los Expositores-->

@extends('admin.layout')

@section('styles')

<link href="{{ asset('adminlte/assets/global/plugins/Jquery-validity/css/jquery.validity.css') }}" rel="stylesheet">
 
@endsection

@section('content')
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="header">
    <h2>Crea un nuevo <strong>Expositor</strong></h2>
    <!--<div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li><a href="#">Make</a>
        </li>
        <li><a href="forms.html">Forms</a>
        </li>
        <li class="active">Input Masks</li>
      </ol>
    </div>-->
  </div>
  <div class="row">
    <div class="col-lg-12 portlets">
      <!--<div class="panel"> -->
      <form class="panel" id="form" method="post" action="{{ route('admin.speakers.store')}}" enctype="multipart/form-data">
      	{{ csrf_field() }}
        <!--<div class="panel-header panel-controls">
          <h3><i class="icon-bulb"></i> Input <strong>Masks</strong></h3>
        </div>-->
        <div class="panel-content">
        @if (session('notification'))
            <div class="alert alert-success">
                <p>{{ session('notification') }}</p>
            </div>
        @endif
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
          <div class="row">
            <div class="col-md-6">
        
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" placeholder="Ingresa el nombre" name="name" id="Nombre" value="{{ old('name') }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Título</label>
                <input type="text" class="form-control" placeholder="Ingresa el título" name="title" id="Titulo" value="{{ old('titulo') }}" >
              </div>

              <div class="form-group">
                <label class="form-label">Descripción</label>
                <textarea name="description" id="Descripcion" rows="10" class="form-control" placeholder="Escriba la descripción del expositor... (maximo 170 caracteres)" maxlength="170">{{ old('description') }}</textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label class="form-label">Imágen</label>
                <input type="file" name="image" id="Imagen" value="{{ old('image') }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Sitio Web</label>
                <input type="text" name="website" id="Website" class="form-control" placeholder="Ej: https://www.mipaginaweb.com" value="{{ old('website') }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Twitter</label>
                <input type="text" name="red1"  class="form-control" placeholder="Ingrese el link de una red social" value="{{ old('red1') }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Facebook</label>
                <input type="text" name="red2"  class="form-control" placeholder="Ingrese el link de una red social" value="{{ old('red2') }}">
              </div>
              <div class="form-group">
                <label class="form-label">Instagram</label>
                <input type="text" name="red3"  class="form-control" placeholder="Ingrese el link de una red social" value="{{ old('red3') }}">
              </div>
            </div>
          </div>
              <button  class="btn btn-primary" type="submit">Registrar Expositor</button>
              <a  class="btn btn-default" href="{{ url('admin') }}">Volver atrás</a>
        </div>

      <!--</div>-->
      </form>
    </div>
  </div>

</div>
<!-- END PAGE CONTENT -->
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('adminlte/assets/global/plugins/Jquery-validity/js/jquery.validity.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminlte/assets/global/js/jquery.validations.js') }}"></script>

@endsection