<!-- 14/07/2018 Lisely fuenmayor: Vista para editar los Expositores-->
@extends('admin.layout')

@section('styles')

<link href="{{ asset('adminlte/assets/global/plugins/Jquery-validity/css/jquery.validity.css') }}" rel="stylesheet">
 
@endsection

@section('content')
<div class="header">
        <h2>Editar datos de <strong>{{ $expositor->name }}</strong></h2>
  </div>
  @if (session('notification'))
            <div class="alert alert-success">
                <p>{{ session('notification') }}</p>
            </div>
  @endif
  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li style="list-style: none;">{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
	<form class="panel" id="form" method="post" action="{{ route('admin.speakers.update') }}" enctype="multipart/form-data" >

        <div class="panel-content">
  {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" id="Nombre" name="name"  value="{{ $expositor->name }}">
              </div>
               <div class="form-group">
                <label class="form-label">Titulo</label>
                <input type="text" class="form-control" id="Titulo" name="title"  value="{{ $expositor->title }}">
              </div>
              <div class="form-group">
                <label class="form-label">Descripción</label>
                <textarea name="description" id="Descripcion" rows="10" class="form-control"  maxlength="170">{{ $expositor->description }}</textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label class="form-label">Imágen</label>
                <input type="file" name="image">
                @if ($expositor->image)
                  <p class="help-block">
                      Subir solo si desea reemplazar la <a href="{{ asset('/images/img-speakers/'.$expositor->image) }}" target="_blank">imágen actual.</a>
                  </p>
                @endif
              </div>
              <div class="form-group">
                <label class="form-label">Sitio Web</label>
                <input type="text" name="website" id="Website" class="form-control"  value="{{ $expositor->website }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Twitter</label>
                <input type="text" name="red1" class="form-control" value="{{ $expositor->red1 }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Facebook</label>
                <input type="text" name="red2"  class="form-control" value="{{ $expositor->red2 }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Instagram</label>
                <input type="text" name="red3"  class="form-control" value="{{ $expositor->red3 }}" >
                <input type="hidden" name="expo" value="{{ $expositor->id }}">
              </div>
            </div>
          </div>
      <button class="btn btn-primary" type="submit">Actualizar Datos</button>
      <a  class="btn btn-default" href="{{ url('admin/speakers') }}">Volver atrás</a>
   </div>
    </form>

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('adminlte/assets/global/plugins/Jquery-validity/js/jquery.validity.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminlte/assets/global/js/jquery.validations.js') }}"></script>

@endsection