<!-- 14/07/2018 Lisely fuenmayor: Vista para ver los datos de  Expositores-->
@extends('admin.layout')

@section('styles')
    <link href="{{ asset('adminlte/assets/global/plugins/magnific/magnific-popup.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/assets/global/plugins/hover-effects/hover-effects.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/assets/global/css/estilos.css') }}" rel="stylesheet">

@endsection

@section('content')


		<div class="header">
		    <h2>Datos de <strong>{{ $expositor->name }}</strong></h2>
	    </div>
      <div class="panel">
        <div class="panel-content">

          <div class="row">
            <div class="col-md-6">

            
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" name="name" readonly value="{{ $expositor->name }}">
              </div>
              <div class="form-group">
                <label class="form-label">Titulo</label>
                <input type="text" class="form-control" name="title" readonly value="{{ $expositor->title }}">
              </div>
              <div class="form-group">
                <label class="form-label">Descripción</label>
                <textarea name="description" rows="10" class="form-control" maxlength="170" readonly>{{ $expositor->description }}</textarea>
              </div>
            </div>
            <div class="col-md-6">

               <div class="portfolioContainer grid">

                <figure class="effect-zoe magnific pic">
                      <img src="{{ $expositor->ImageUrl }}" alt="Expositor"/>
                      <figcaption>
                        <h6> <span>{{$expositor->name}}</span></h6>
                        <p>{{$expositor->descripcion}}</p>
                        
                        
                      </figcaption>
               </figure>

                </div>

         
            </div>

           <div class="row">

             <div class="col-md-6">

              <div class="form-group">
                <label class="form-label">Sitio Web</label>
                <input type="text" name="website" class="form-control" readonly value="{{ $expositor->website }}">
              </div>
              <div class="form-group">
                <label class="form-label">Twitter</label>
                <input type="text" name="red1" class="form-control" value="{{ $expositor->red1 }}" readonly>
              </div>


             </div>
             <div class="col-md-6">

                   <div class="form-group">
                <label class="form-label">Facebook</label>
                <input type="text" name="red2" required class="form-control" value="{{ $expositor->red2 }}" readonly>
              </div>
              <div class="form-group">
                <label class="form-label">Instagram</label>
                <input type="text" name="red3" required class="form-control" value="{{ $expositor->red3 }}" readonly>
              </div>

             </div>
            
          </div>
          </div>
      <a  class="btn btn-default" href="{{ url('admin/speakers') }}">Volver atrás</a>
    </div>
  </div>
@endsection