<ul class="nav nav-sidebar">
	<li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{route('dashboard')}}"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
	<li class="nav-parent {{ Request::is('admin/client*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-group"></i><span>Clientes</span> <span class="fa arrow"></span></a>
	  <ul class="children collapse">
		<li {{ Request::is('admin/client') ? 'class=active' : '' }}><a href="{{ route('admin.clients.index')}}">Ver Clientes</a></li>
		<li {{ Request::is('admin/client/create') ? 'class=active' : '' }}><a href="{{ route('admin.clients.create')}}">Crear Cliente</a></li>
	  </ul>
	</li>
<!-- 	<li class="nav-parent {{ Request::is('admin/provider*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-book"></i><span>Proveedores</span><span class="fa arrow"></span></a>
	  <ul class="children collapse">
		<li {{ Request::is('admin/provider') ? 'class=active' : '' }}><a href="{{ route('admin.providers.index')}}">Ver Proveedores</a></li>
		<li {{ Request::is('admin/provider/create') ? 'class=active' : '' }}><a href="{{ route('admin.providers.create')}}">Crear Proveedor</a></li>
	  </ul>
	</li> -->
	<li class="nav-parent {{ Request::is('admin/post*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-th-list"></i><span>Publicaciones</span><span class="fa arrow"></span></a>
	  <ul class="children collapse">
		<li {{ Request::is('admin/post') ? 'class=active' : '' }}><a href="{{ route('admin.posts.index')}}">Ver Publicaciones</a></li>
		<li {{ Request::is('admin/post/create') ? 'class=active' : '' }}><a href="{{ route('admin.posts.create')}}">Crear Publicaciones</a></li>
		{{-- <li {{ Request::is('admin/tags') ? 'class=active' : '' }}><a href="{{ route('admin.tags.index')}}">Ver Etiquetas</a></li>
		<li {{ Request::is('admin/categories') ? 'class=active' : '' }}><a href="{{ route('admin.categoies.index')}}">Ver Categorías</a></li> --}}
	  </ul>
	</li> 
	<li class="nav-parent {{ Request::is('admin/user*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-user"></i><span>Usuarios</span><span class="fa arrow"></span></a>
	  <ul class="children collapse">
			<li ><a href="{{ route('indexUsuario')}}">Ver Usuarios</a></li>
			<li ><a href="{{ route('createUser')}}">Crear Usuarios</a></li>
	  </ul>
	</li>
	<!--Speakers-->
	<li class="nav-parent {{ Request::is('admin/speakers*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-user"></i><span>Expositores</span><span class="fa arrow"></span></a>
	  <ul class="children collapse">
		<li {{ Request::is('admin/speakers') ? 'class=active' : '' }}><a href="{{ route('admin.speakers.index')}}">Ver Expositores</a></li>
		<li {{ Request::is('admin/speakers/create') ? 'class=active' : '' }}><a href="{{ route('admin.speakers.create')}}">Crear Expositor</a></li>
	  </ul>
	</li>

		<!--Sponsors-->
	<li class="nav-parent {{ Request::is('admin/sponsors*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-group"></i><span>Sponsors</span><span class="fa arrow"></span></a>
	  <ul class="children collapse">
		<li {{ Request::is('admin/sponsors') ? 'class=active' : '' }}><a href="{{ route('admin.sponsors.index')}}">Ver Sponsors</a></li>
		<li {{ Request::is('admin/sponsors/create') ? 'class=active' : '' }}><a href="{{ route('admin.sponsors.create')}}">Crear Sponsors</a></li>
	  </ul>
	</li>

		<!--Eventos-->
	<li class="nav-parent {{ Request::is('admin/events*') ? 'active' : '' }}">
	  <a href="#"><i class="fa fa-book"></i><span>Eventos</span><span class="fa arrow"></span></a>
	  <ul class="children collapse">
		<li {{ Request::is('admin/events') ? 'class=active' : '' }}><a href="{{ route('admin.events.index')}}">Ver Eventos</a></li>
		<li {{ Request::is('admin/events/create') ? 'class=active' : '' }}><a href="{{ route('admin.events.create')}}">Crear Eventos</a></li>
	  </ul>
	</li>

</ul>
