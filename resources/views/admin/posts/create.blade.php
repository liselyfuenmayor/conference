@extends('admin.layout')

@section('header')
    <div class="header">
      <h2><strong>Lista de publicaciones</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="{{ route('dashboard')}}">Inicio</a>
          </li>
          <li><a href="{{ route('admin.posts.index')}}">Posts</a>
          </li>
          <li class="active">Crear</li>
        </ol>
      </div>
    </div>
@stop


@section('content')
<form method="POST" action="{{ route('admin.posts.store')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-8 ">
          <div class="panel">
            <div class="panel-header">
              <h3><i class="icon-bulb"></i> <strong>Titles</strong> options</h3>
              
            </div>
            <div class="panel-content">
              <div class="row">
                <div class="section-title">
                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="form-label"><strong>Título</strong></label>
                        <input name="title" placeholder="Ingresa un título para la publicación" class="form-control form-white">
                          {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
                        <label class="form-label"><strong>Contenido</strong></label>
                        <textarea id="editor1" rows="10" name="body" class="summernote bg-white form-control" placeholder="Ingresa el contenido de la publicación"></textarea>
                        {!! $errors->first('body', '<span class="help-block">:message</span>') !!}
                    </div>
                     <!-- Adicion de la imagen a las publicaciones -->
                    <div class="form-group">
                     <label class="form-label">Imágen</label>
                      <input type="file" name="image" id="Imagen" value="{{ old('image') }}" >
                    </div>

                </div>
                 
                  </div>
            </div>
          </div>
        </div>
      
        <div class="col-lg-4 ">
          <div class="panel">
            {{-- <div class="panel-header">
              <h3><i class="icon-bulb"></i> <strong>Lists</strong> ordered, unordered, customized</h3>
              
            </div> --}}
            <div class="panel-content">
              <div class="row">
                <div class="section-title">
                  <div class="form-group {{ $errors->has('excerpt') ? ' has-error' : '' }}">
                        <label class="form-label"><strong>Extracto</strong></label>
                        <input name="excerpt" placeholder="Ingresa un resumen de la publicación" class="form-control form-white">
                         {!! $errors->first('excerpt', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                      <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
                          <label><strong>Categorías</strong></label>
                          <select name="category" class="form-control" data-placeholder="Seleccione una categoría">
                              <option value="">Selecciona Una Categoría</option>
                              @foreach ($categories as $category)   
                                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                              @endforeach
                          </select>
                          {!! $errors->first('category', '<span class="help-block">:message</span>') !!}
                      </div>
                    <div class="form-group">
                        <label><strong>Etiquetas</strong></label>
                        <select name="tags[]" class="form-control select2" multiple data-placeholder="Seleccione etiquetas">
                            @foreach ($tags as $tag)
                               <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                            
                            
                        </select>
                        
                    </div>
                    <div class="form-group">
                        <label class="form-label">Fecha de Publicación</label>
                        <div class="prepend-icon">
                          <input type="text" name="published_at" class="date-picker form-control form-white" placeholder="Seleccione una fecha">
                          <i class="icon-calendar"></i>
                        </div>
                      </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-lg btn-block btn-success"><i class="fa fa-save pull-left"></i>Guardar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
</form>
    
@stop

@push('styles')
    <link href="/adminlte/assets/global/plugins/summernote/summernote.min.css" rel="stylesheet">
@endpush

@push('scripts')
    <script src="/adminlte/assets/global/plugins/summernote/summernote.min.js"></script> <!-- Simple HTML Editor -->
    <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script> <!-- Advanced HTML Editor -->
    <script src="/adminlte/assets/global/plugins/typed/typed.min.js"></script> <!-- Animated Typing -->
    <script src="/adminlte/assets/global/js/pages/editor.js"></script>
    <script>
      
    </script>
    <script>
        $('#datepicker').datepicker({
            autoclose: true,
            language: "es",
        });
        CKEDITOR.replace('editor1');
    </script>
@endpush
