@extends('admin.layout')

@section('header')
    <div class="header">
      <h2><strong>Todas las Publicaciones</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li class=""><a href="{{ route('dashboard')}}">Inicio</a>
          </li>
          <li class="active"><a href="#">Posts</a>
          </li>
          {{-- <li class="active">Typography</li> --}}
        </ol>
      </div>
    </div>
@endsection

@section('content')
    <div class="row">
            <div class="col-lg-12 portlets">
              <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="fa fa-table"></i> <strong>Export Tools</strong> options <small>export to Excel, CSV, PDF or Print.</small></h3>
                </div>
                <div class="panel-content">
                  <div class="filter-left">
                    <table class="table table-dynamic table-tools" data-table-name="Total users">
                      <thead>
                        <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Extracto</th>
                        <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach ($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->excerpt }}</td>
                            <td>
                                <a href="#" class="btn btn-xs btn-blue"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                            </td>
                        </tr>
                    @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection

{{-- @section('content')
	    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Listado de Posts</strong>
                        </div>
                        <div class="card-body">
                  <table id="posts-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Extracto</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    @foreach ($posts as $post)
                    	<tr>
                    		<td>{{ $post->id }}</td>
                    		<td>{{ $post->title }}</td>
                    		<td>{{ $post->excerpt }}</td>
                    		<td>
                    			<a href="#" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                    			<a href="#" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                    			<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                    		</td>
                    	</tr>
                    @endforeach
                    <tbody>
                 
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@stop --}}


@push('styles')
	<link href="/adminlte/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="/adminlte/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
@endpush

@push('scripts')
	
	<script src="/adminlte/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
    <script src="/adminlte/assets/global/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/adminlte/assets/global/js/pages/table_dynamic.js"></script>
	{{-- <script>
	$(document).ready(function() {
        $('#posts-table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );
	</script> --}}
	@endpush