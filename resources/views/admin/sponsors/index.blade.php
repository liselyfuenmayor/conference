<!-- 14/07/2018 Lisely fuenmayor: Vista para ver el listado de los Sponsors-->
@extends('admin.layout')

@section('content')
<div class="header">
    <h2>Lista de <strong>Sponsors</strong></h2>
</div>
<div class="panel">
   <!-- <div class="panel-header panel-controls">
      <h3><i class="fa fa-table"></i> <strong>Striped rows</strong> Table</h3>
    </div> -->
    <div class="panel-content">

      @if (session('notification'))
            <div class="alert alert-success">
                <p>{{ session('notification') }}</p>
            </div>
        @endif

          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

      <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Descripción</th>   
            <th>Clase</th>        
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($sponsors as $sponsor)
            <tr>
                <td>{{ $sponsor->id }}</td>
                <td>{{ $sponsor->name }}</td>
                <td>{{ $sponsor->description }}</td>
                <td>{{ $sponsor->className }}</td>
                
                <td>
                  <form method="post" action="{{ url('admin/sponsors/delete') }}">
                      {{ csrf_field() }}
                      <div style="display: inline-flex;">
                      <a href="{{ url('admin/sponsors/'.$sponsor->id) }}"  rel="tooltip"  title="Ver información" class="btn btn-sm btn-blue">
                        <i class="fa fa-info"></i>
                      </a>

                      <a href="{{ url('admin/sponsors/edit/'.$sponsor->id) }}" rel="tooltip" title="Editar Producto" class="btn btn-sm btn-primary">
                          <i class="fa fa-edit"></i>
                      </a>

                      <input type="hidden" name="id" value="{{ $sponsor->id }}">
                      <button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
                           <i class="fa fa-times"></i>
                      </button>
                      </div>
                  </form>
              </td>
            </tr>
        @endforeach
        </tbody>
      </table>
      {{ $sponsors->links() }}
    </div>
</div>
@endsection