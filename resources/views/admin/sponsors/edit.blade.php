<!-- 14/07/2018 Lisely fuenmayor: Vista para editar los sponsors-->
@extends('admin.layout')

@section('styles')

<link href="{{ asset('adminlte/assets/global/plugins/Jquery-validity/css/jquery.validity.css') }}" rel="stylesheet">
 
@endsection


@section('content')
<div class="header">
        <h2>Editar datos de <strong>{{ $sponsor->name }}</strong></h2>
  </div>
	<form class="panel" id="form" method="post" action="{{ url('admin/sponsors/update')}} " enctype="multipart/form-data" >

   @if (session('notification'))
            <div class="alert alert-success">
                <p>{{ session('notification') }}</p>
            </div>
        @endif
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

  {{ csrf_field() }}
    <div class="panel-content">
          <div class="row">
            <div class="col-md-6">
             
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" name="name" id="Nombre"  value="{{ $sponsor->name }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Título</label>
                <input type="text" class="form-control" name="title" id="Título"  value="{{ $sponsor->title }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Descripción</label>
                <textarea name="description" id="Descripcion" rows="10" class="form-control"  maxlength="170" >{{ $sponsor->description }}</textarea>
              </div>
            </div>
            <div class="col-md-6">

               <div class="form-group">
                  <label class="form-label">Imágen</label>
                <input type="file" name="image" >
                @if ($sponsor->image)
                  <p class="help-block">
                      Subir solo si desea reemplazar la <a href="{{ asset('/images/img-sponsors/'.$sponsor->image) }}" target="_blank">imágen actual.</a>
                  </p>
                @endif
              </div>

                @if ($sponsor->class == 1)
               <div class="form-group">
                <label class="control-label">Clase</label>
                <div class="option-group" >
                  <select id="clase" name="clase" class="form-control"  >
                    <option value="">Seleccione la clase</option>
                    <option value="1" name="clase" selected>Oficial</option>
                    <option value="2" name="clase">Oro</option>
                    <option value="3" name="clase">Plata</option>
                  </select>
                </div>
               </div>
               @else

                  @if ($sponsor->class == 2)

                  <div class="form-group">
                  <label class="control-label">Clase</label>
                  <div class="option-group" >
                    <select id="clase" name="clase" class="form-control" >
                      <option value="">Seleccione la clase</option>
                      <option value="1" name="clase" >Oficial</option>
                      <option value="2" name="clase " selected>Oro</option>
                      <option value="3" name="clase">Plata</option>
                    </select>
                  </div>
                 </div>

                 @else

                 <div class="form-group">
                  <label class="control-label">Clase</label>
                  <div class="option-group" >
                    <select id="clase" name="clase" class="form-control" required>
                      <option value="">Seleccione la clase</option>
                      <option value="1" name="clase" >Oficial</option>
                      <option value="2" name="clase " >Oro</option>
                      <option value="3" name="clase" selected>Plata</option>
                    </select>
                  </div>
                 </div>


                  @endif




              @endif

              <div class="form-group">
                <label class="form-label">Sitio Web</label>
                <input type="text" name="website" id="Website" class="form-control"  value="{{ $sponsor->website }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Twitter</label>
                <input type="text" name="red1" class="form-control" value="{{ $sponsor->red1 }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Facebook</label>
                <input type="text" name="red2"  class="form-control" value="{{ $sponsor->red2 }}" >
              </div>
              <div class="form-group">
                <label class="form-label">Instagram</label>
                <input type="text" name="red3"  class="form-control" value="{{ $sponsor->red3 }}" >
                <input type="hidden" name="expo" value="{{ $sponsor->id }}">
              </div>

             <input type="hidden" name="id" value="{{ $sponsor->id }}">
            
            </div>
          </div>
      <button class="btn btn-primary" type="submit">Actualizar Datos</button>
      <a  class="btn btn-default" href="{{ url('admin/sponsors') }}">Volver atrás</a>
      </div>
    </form>

 
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('adminlte/assets/global/plugins/Jquery-validity/js/jquery.validity.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminlte/assets/global/js/jquery.validations.js') }}"></script>

@endsection