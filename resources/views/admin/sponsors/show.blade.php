<!-- 14/07/2018 Lisely fuenmayor: Vista para ver los datos de los Sponsors-->
@extends('admin.layout')

@section('styles')
    <link href="{{ asset('adminlte/assets/global/plugins/magnific/magnific-popup.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/assets/global/plugins/hover-effects/hover-effects.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/assets/global/css/estilos.css') }}" rel="stylesheet">

@endsection


@section('content')

	
		<div class="header">
		    <h2>Datos de <strong>{{ $sponsor->name }}</strong></h2>
	    </div>
      <div class="panel">
       <div class="panel-content">
          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" name="name" readonly value="{{ $sponsor->name }}">
              </div>
              <div class="form-group">
                <label class="form-label">Titulo</label>
                <input type="text" class="form-control" name="title" readonly value="{{ $sponsor->title }}">
              </div>
              <div class="form-group">
                <label class="form-label">Descripción</label>
                <textarea name="description" rows="10" class="form-control" maxlength="170" readonly>{{ $sponsor->description }}</textarea>
              </div>
            </div>
            <div class="col-md-6">
                <div class="portfolioContainer grid">
                    <figure class=" effect-zoe magnific pic" >
                      <img src="{{ $sponsor->ImageUrl }}"   alt="Sponsor"/>
                      <figcaption>
                        <h6> Clase: <span>{{ $sponsor->ClassName }}</span></h6>
                        
                        <p>{{ $sponsor->name }}</p>
                      </figcaption>
                    </figure>
                </div>
 
       <div class="row">

             <div class="col-md-6">

              <div class="form-group">
                <label class="form-label">Sitio Web</label>
                <input type="text" name="website" class="form-control" readonly value="{{ $sponsor->website }}">
              </div>
              <div class="form-group">
                <label class="form-label">Twitter</label>
                <input type="text" name="red1" class="form-control" value="{{ $sponsor->red1 }}" readonly>
              </div>


             </div>
             <div class="col-md-6">

                   <div class="form-group">
                <label class="form-label">Facebook</label>
                <input type="text" name="red2" required class="form-control" value="{{ $sponsor->red2 }}" readonly>
              </div>
              <div class="form-group">
                <label class="form-label">Instagram</label>
                <input type="text" name="red3" required class="form-control" value="{{ $sponsor->red3 }}" readonly>
              </div>

             </div>
            
          </div>
             
            </div>
          </div>
      <a  class="btn btn-default" href="{{ url('admin/sponsors') }}">Volver atrás</a>
     
    </div>
      </div>

       
@endsection
