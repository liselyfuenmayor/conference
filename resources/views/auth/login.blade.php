<!DOCTYPE html>
<html>
    
<!-- Mirrored from themes-lab.com/make/admin/layout1/user-login-v1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Jul 2018 03:20:47 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        <meta charset="utf-8">
        <title>Smartpc | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="/adminlte/assets/global/images/favicon.png">
        <link href="/adminlte/assets/global/css/style.css" rel="stylesheet">
        <link href="/adminlte/assets/global/css/ui.css" rel="stylesheet">
        <link href="/adminlte/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>

    <body class="account separate-inputs" data-page="login">
        <br><br><br>
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">

                        <i class="user-img icons-faces-users-03"></i>
                        <form class="form-signin" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} append-icon">
                                <input type="email" 
                                    class="form-control form-white username" 
                                    placeholder="Email" 
                                    name="email" 
                                    value="{{ old('email') }}" required autofocus>
                                <i class="fa fa-envelope-o"></i>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} append-icon m-b-20">
                                <input type="password" name="password" class="form-control form-white password" placeholder="Contraseña" required>
                                <i class="icon-lock"></i>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Ingresar</button>
                            <div class="clearfix">
                                <p class="pull-left m-t-20"><a id="password" href="#">¿Olvidó su Contraseña?</a></p>
                            </div>
                        </form>
                        {{-- <form class="form-password" role="form">
                            <div class="append-icon m-b-20">
                                <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                                <i class="icon-lock"></i>
                            </div>
                            <button type="submit" id="submit-password" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Send Password Reset Link</button>
                            <div class="clearfix">
                                <p class="pull-left m-t-20"><a id="login" href="#">Already got an account? Sign In</a></p>
                                <p class="pull-right m-t-20"><a href="user-signup-v1.html">New here? Sign up</a></p>
                            </div>
                        </form> --}}
                    </div>
                </div>
            </div>
            <p class="account-copyright">
                <span>Copyright © 2018 </span><span>Smartpc Ecuador</span>.<span> Todos los derechos reservados.</span>
            </p>
        </div>
        <!-- END LOGIN BOX -->

       <script src="/adminlte/assets/global/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="/adminlte/assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
        <script src="/adminlte/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="/adminlte/assets/global/plugins/tether/js/tether.min.js"></script>
        <script src="/adminlte/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/adminlte/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="/adminlte/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="/adminlte/assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/adminlte/assets/global/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="/adminlte/assets/global/js/pages/login-v1.js"></script>
    </body>

<!-- Mirrored from themes-lab.com/make/admin/layout1/user-login-v1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Jul 2018 03:20:50 GMT -->
</html>

