<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('roles')->insert([
            'name' => 'Super Administrador',
        ]);

         DB::table('roles')->insert([
            'name' => 'Administrador',
        ]);

         DB::table('roles')->insert([
            'name' => 'Usuario',
        ]);

        DB::table('roles')->insert([
            'name' => 'Proveedor',
        ]);

        DB::table('roles')->insert([
            'name' => 'Cliente',
        ]);

        $user = new User;
        $user->username = 'smartpc';
        $user->name = 'Juan Fernando';
        $user->document_number = '172326428';
        $user->lastname = 'Lema';
        $user->email = 'admin@admi n.com';
        $user->address = 'La forestal';
        $user->phone = '3123441';
        $user->password = bcrypt('123456');
        $user->role_id = 1;
        $user->status_id = 1;
        $user->type_document_id = 1;
        $user->save();

   

        $user = new User;
        $user->username = 'Juan';
        $user->name = 'Juan';
        $user->document_number = '172023428';
        $user->lastname = 'Fher';
        $user->email = 'juan@admin.com';
        $user->address = 'La forestal';
        $user->phone = '3123441';
        $user->password = bcrypt('123456');
        $user->role_id = 2;
        $user->status_id = 1;
        $user->type_document_id = 1;
        $user->save();

        $user = new User;
        $user->username = 'fher';
        $user->name = 'Fernando';
        $user->document_number = '17203428';
        $user->lastname = 'Fher';
        $user->email = 'fher@admin.com';
        $user->address = 'La forestal';
        $user->phone = '3123441';
        $user->password = bcrypt('123456');
        $user->role_id = 2;
        $user->status_id = 1;
        $user->type_document_id = 3;
        $user->save();

/*Usuario añadido al proyecto*/
        $user = new User;
        $user->username = 'Lfuenmayor';
        $user->name = 'Lisely ';
        $user->document_number = '134436428';
        $user->lastname = 'Fuenmayor';
        $user->email = 'liselycarolina@admin.com';
        $user->address = 'Altamira';
        $user->phone = '04140665484';
        $user->password = bcrypt('123456');
        $user->role_id = 1;
        $user->status_id = 1;
        $user->type_document_id = 3;
        $user->save();
    }
}
