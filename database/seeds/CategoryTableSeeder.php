<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        $category = new Category;
        $category->name = 'Categoria 1';
        $category->save();
        
        $category = new Category;
        $category->name = 'Categoria 2';
        $category->save();
        
        $category = new Category;
        $category->name = 'Categoria 3';
        $category->save();

        DB::table('tags')->insert([
            'name' => 'Perros',
        ]);
        DB::table('tags')->insert([
            'name' => 'gatos',
        ]);
        DB::table('tags')->insert([
            'name' => 'loros',
        ]);
    }
}
