<?php

use App\Post;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new Post;
        $post->user_id = 3;
        $post->title = 'Mi Primer Post';
        $post->excerpt = '<p>Aunque verás que hay muchos artículos que ya explican</p>';
        $post->body = '<p>Bueno, el caso es que como te decía al inicio del post, un blog es realmente una página web. Pero la diferencia con el resto de páginas web “normales” es que se centra en mostrar publicaciones (llamadas posts o artículos) como si fuera un diario personal.</p>';
        $post->published_at = Carbon::now()->subDays(5);
        $post->category_id = 1;
        $post->save();

        $post = new Post;
        $post->user_id = 3;
        $post->title = 'Mi segundo Post';
        $post->excerpt = '<p>Aunque verás que hay muchos artículos que ya explican</p>';
        $post->body = '<p>Bueno, el caso es que como te decía al inicio del post, un blog es realmente una página web. Pero la diferencia con el resto de páginas web “normales” es que se centra en mostrar publicaciones (llamadas posts o artículos) como si fuera un diario personal.</p>';
        $post->published_at = Carbon::now()->subDays(4);
        $post->category_id = 2;
        $post->save();

        $post = new Post;
        $post->user_id = 3;
        $post->title = 'Mi tercer Post';
        $post->excerpt = '<p>Aunque verás que hay muchos artículos que ya explican </p>';
        $post->body = '<p>Bueno, el caso es que como te decía al inicio del post, un blog es realmente una página web. Pero la diferencia con el resto de páginas web “normales” es que se centra en mostrar publicaciones (llamadas posts o artículos) como si fuera un diario personal.</p>';
        $post->published_at = Carbon::now()->subDays(3);
        $post->category_id = 3;
        $post->save();

        $post = new Post;
        $post->user_id = 3;
        $post->title = 'Mi cuarto Post';
        $post->excerpt = '<p>Aunque verás que hay muchos artículos que ya explican </p>';
        $post->body = '<p>Bueno, el caso es que como te decía al inicio del post, un blog es realmente una página web. Pero la diferencia con el resto de páginas web “normales” es que se centra en mostrar publicaciones (llamadas posts o artículos) como si fuera un diario personal.</p>';
        $post->published_at = Carbon::now()->subDays(2);
        $post->category_id = 2;
        $post->save();

        $post = new Post;
        $post->user_id = 3;
        $post->title = 'Mi quinto Post';
        $post->excerpt = '<p>Aunque verás que hay muchos artículos que ya explican</p>';
        $post->body = '<p>Bueno, el caso es que como te decía al inicio del post, un blog es realmente una página web. Pero la diferencia con el resto de páginas web “normales” es que se centra en mostrar publicaciones (llamadas posts o artículos) como si fuera un diario personal.</p>';
        $post->published_at = Carbon::now()->subDays(1);
        $post->category_id = 1;
        $post->save();

        DB::table('post_tag')->insert([
            'post_id' => 1,
            'tag_id' => 1,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 1,
            'tag_id' => 2,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 1,
            'tag_id' => 1,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 2,
            'tag_id' => 2,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 2,
            'tag_id' => 3,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 3,
            'tag_id' => 1,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 3,
            'tag_id' => 3,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 4,
            'tag_id' => 1,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 4,
            'tag_id' => 2,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 4,
            'tag_id' => 3,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 5,
            'tag_id' => 1,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 6,
            'tag_id' => 2,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 7,
            'tag_id' => 3,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 7,
            'tag_id' => 1,
        ]);
        DB::table('post_tag')->insert([
            'post_id' => 8,
            'tag_id' => 1,
        ]);
        

    }
}
