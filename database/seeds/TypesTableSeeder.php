<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types_accountings')->insert([
            'name' => 'No Obligado',
        ]);
        DB::table('types_accountings')->insert([
            'name' => 'Obligado',
        ]);

        DB::table('types_bills')->insert([
            'name' => 'Producto',
        ]);
        DB::table('types_bills')->insert([
            'name' => 'alicuota',
        ]);
        DB::table('types_bills')->insert([
            'name' => 'adicionales',
        ]);
        DB::table('types_bills')->insert([
            'name' => 'multas',
       ]);
       DB::table('types_payments')->insert([
            'name' => 'Producto',
        ]);
       DB::table('types_payments')->insert([
            'name' => 'Servicio',
        ]);

       DB::table('types_providers')->insert([
            'name' => 'Natural',
        ]);
        DB::table('types_providers')->insert([
            'name' => 'Juridica',
        ]);
        DB::table('types_providers')->insert([
            'name' => 'Profesional'
        ]);
        DB::table('types_providers')->insert([
            'name' => 'RISE'
        ]);

        DB::table('types_retentions_taxs')->insert([
            'value' => 0,
        ]);
        DB::table('types_retentions_taxs')->insert([
            'value' => 30,
        ]);
        DB::table('types_retentions_taxs')->insert([
            'value' => 70,
        ]);
        DB::table('types_retentions_taxs')->insert([
            'value' => 100,
        ]);

        DB::table('types_retentions_sources')->insert([
            'value' => 1,
        ]);
        DB::table('types_retentions_sources')->insert([
            'value' => 2,
        ]);
        DB::table('types_retentions_sources')->insert([
            'value' => 8,
        ]);
        DB::table('types_retentions_sources')->insert([
            'value' => 100,
        ]);

    }
}
