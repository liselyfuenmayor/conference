<?php

use App\People;
use App\Type_Provider;
use App\Type_Accounting;
use Illuminate\Database\Seeder;

class PeoplesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $people = new People;
        $people->role_id = 5;
        $people->name = 'SmartPc Ecuador';
        $people->document_number = '1720736428001';
        $people->document_type = 'CEDULA';
        $people->phone = '3123441';
        $people->home_phone = '3123441';
        $people->address = 'La forestal';
        $people->email = 'admin@admin.com';
        // $people->type_accounting_id = 1;
        // $people->type_provider = ;
        $people->status = 1;
        $people->save();

        $people = new People;
        $people->role_id = 4;
        $people->name = 'SmartPc';
        $people->document_number = '1720736428001';
        $people->document_type = 'RUC';
        $people->phone = '3123441';
        $people->home_phone = '3123441';
        $people->address = 'La forestal';
        $people->email = 'smartpc@admin.com';
        $people->type_accounting_id = 1;
        $people->type_provider_id = 1;
        $people->status = 1;
        $people->save();

        // $people = new People;
        // $people->role_id = 4;
        // $people->name = 'Ecuador';
        // $people->document_number = '1720736428001';
        // $people->document_type = 'RUC';
        // $people->phone = '3123441';
        // $people->home_phone = '3123441';
        // $people->address = 'La forestal';
        // $people->email = 'admin@admin.com';
        // $people->type_accounting_id = 2;
        // $people->type_provider = 2;
        // $people->status = 1;
        // $people->save();

        // $people = new People;
        // $people->role_id = 5;
        // $people->name = 'VERITO LEMA';
        // $people->document_number = '1720736428001';
        // $people->document_type = 'CEDULA';
        // $people->phone = '3123441';
        // $people->home_phone = '3123441';
        // $people->address = 'La forestal';
        // $people->email = 'admin@admin.com';
        // $people->type_accounting_id = 1;
        // $people->type_provider = 4;
        // $people->status = 1;
        // $people->save();

    }
}
