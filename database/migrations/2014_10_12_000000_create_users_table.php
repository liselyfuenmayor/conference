<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 40);
            $table->string('name', 80);
            $table->string('document_number', 20); //clientes
            $table->string('lastname', 80);
            $table->string('email');
            $table->string('address',120);
            $table->string('phone', 20);
            $table->string('home_phone', 20)->nullable();//clientes
            $table->string('password');
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('status_id');//clientes
            $table->unsignedInteger('type_document_id');//clientes
            $table->foreign('role_id')->references('id')->on('roles');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
