<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSitewebSponsor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sponsors', function (Blueprint $table) {

            $table->string('website')->nullable();
            $table->string('red1')->nullable();
            $table->string('red2')->nullable();
            $table->string('red3')->nullable();
            $table->string('title');

          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('sponsors', function (Blueprint $table) {
          
            $table->dropColumn('website');
            $table->dropColumn('red1');
            $table->dropColumn('red2');
            $table->dropColumn('red3');
            $table->dropColumn('title');
          
         });
    }
}
