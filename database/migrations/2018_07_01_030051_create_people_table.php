<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('role_id');
            $table->string('name', 100);
            $table->string('document_number', 20);
            $table->string('document_type', 20);
            $table->string('phone', 20)->nullable();
            $table->string('home_phone', 20)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('email')->nullable();
            $table->unsignedInteger('type_accounting_id')->nullable();
            $table->unsignedInteger('type_provider_id')->nullable();
            $table->unsignedInteger('status');
            $table->timestamps();
            $table->foreign('type_accounting_id')->references('id')->on('types_accountings')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('type_provider_id')->references('id')->on('types_providers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
