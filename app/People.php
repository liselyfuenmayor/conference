<?php

namespace App;

use App\Permisson;
use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $guarded = [];

    public function type_accounting()
    {
        return $this->belongsTo(Permisson::class);
    }

    public function type_provider()
    {
        return $this->belongsTo(Permisson::class);
    }
}
