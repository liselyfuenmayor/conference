<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeDocument extends Model
{
    protected $table = 'status';
    protected $primaryKey = 'id';
    protected $fillable = ['id','name'];
    public $timestamps = false;



}
