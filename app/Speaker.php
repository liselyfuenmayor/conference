<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria

class Speaker extends Model
{
   
   //Método para el borrado logico

    use SoftDeletes; //Implementamos 

    protected $dates = ['deleted_at']; //Registramos la nueva columna

    protected $fillable = ['name', 'content'];

   // Fin 

   // Relaciones del modelo

       public function event()
    {
        return $this->hasMany(Event::class); //Un expositor tiene muchos eventos  
    }

  // Fin 

//Atributo cálculado ImageUrl : Sirve para cargar la  ruta completa de la imagen

 public function getImageUrlAttribute()
{
    $image= $this->image; 
    if($image){
            $image= '/images/img-speakers/'.$image; 
            return $image; //url = Accesor definido en el modelo Speaker
       }

    //default
    return '/images/default.jpg';
}

//fin

}
