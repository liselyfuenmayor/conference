<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria
use Carbon\Carbon;

class Event extends Model
{

	//Método para el borrado logico

    use SoftDeletes; //Implementamos 

    protected $dates = ['deleted_at']; //Registramos la nueva columna

    protected $fillable = ['name', 'content'];

   // Fin 

	// Relaciones del modelo 
   public function speaker()
    {
      
        return $this->belongsTo(Speaker::class,'idspeaker'); // Un evento Pertenece a un Expositor 
    }
  // Fin

  // Atributo cálculado para obtener el dia de la semana }

   public function getEventDayAttribute()
   {
      
      $fecha = $this->day;

      $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');

      $dia = $dias[date('w', strtotime($fecha))];
      return $dia;
   }

 

}
