<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];
	
    protected $dates = ['published_at'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

//Atributo cálculado ImageUrl : Sirve para cargar la  ruta completa de la imagen

 public function getImageUrlAttribute()
{
    $image= $this->image; 
    if($image){
            $image= '/images/img-posts/'.$image; 
            return $image; //url = Accesor definido en el modelo Speaker
       }

    //default
    return '/images/default.jpg';
}

//fin
}
