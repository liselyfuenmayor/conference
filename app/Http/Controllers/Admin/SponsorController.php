<?php
/*14/07/2018 Lisely Fuenmayor*/
/*SponsorController, es el controlador para el maestro de Sponsors*/

namespace App\Http\Controllers\Admin;

use App\Sponsor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;


class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sponsors = Sponsor::paginate(10);
        return view('admin.sponsors.index')->with(compact('sponsors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sponsors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
          // Validaciones de Backend 

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del sponsor.',
            'image.required' => 'Es necesario ingresar la imagen del sponsor.',
            'description.required' => 'Es necesario ingresar la descripción.',
            'description.max' => 'La descripción admite hasta 170 caracteres como máximo.',
            'clase.required' => 'Debe seleccionar una clase',
            'website.required' => 'Es necesario ingresar el sitio web.',
            'title.required' => 'Es necesario ingresar un título del sponsor'

            
        ];
        
        $rules = [
            'image' => 'required',
            'name' => 'required', //que sea requerido
            'description' => 'required', //que sea requerido y como maximo debe tener 170 caracteres
            'clase' => 'required',
            'website' => 'required',
            'title' => 'required'
        ];

        $this->validate($request, $rules, $messages );

        $sponsor = new Sponsor();

        //Guardar la img en nuestro proyecto
        $file = $request->file('image');
        $path = public_path() . '/images/img-sponsors';
        $filename = uniqid() . $file->getClientOriginalName();
        $moved = $file->move($path, $filename);

        if ($moved) // Si la imagen se guardo , lo registra en base de datos
        {
            $sponsor->image = $filename;
        }
   

        $sponsor->name = $request->input('name');
        $sponsor->description = $request->input('description');
        $sponsor->class = $request->input('clase');
        $sponsor->website = $request->input('website');
        $sponsor->red1 = $request->input('red1');
        $sponsor->red2 = $request->input('red2');
        $sponsor->red3 = $request->input('red3');
        $sponsor->title = $request->input('title');
        $save = $sponsor->save();

        if ($save) // si el registro fue guardado con exito, envia la notificación
        {
        $notification = "Se ha creado correctamente el Sponsor";
        return back()->with(compact('notification'));
        } 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function show(Sponsor $sponsor, $id)
    {
        $sponsor = Sponsor::find($id);
        return view('admin.sponsors.show')->with(compact('sponsor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function edit(Sponsor $sponsor, $id)
    {
        $sponsor = Sponsor::find($id);
        return view('admin.sponsors.edit')->with(compact('sponsor'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sponsor $sponsor)
    {
        $save2 = false; // Inicializa la variable save2 en falso, la misma que identifica si se guardo la imagen 

         //Validaciones de Backend
        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del sponsor.',
            'description.required' => 'Es necesario ingresar la descripción.',
            'description.max' => 'La descripción admite hasta 170 caracteres como máximo.',
            'clase.required' => 'Debe seleccionar una clase',
            'title.required' => 'Es necesario ingresar un título del sponsor'
            
        ];
        
        $rules = [
            
            'name' => 'required', //que sea requerido
            'description' => 'required|max:170', //que sea requerido y como maximo debe tener 170 caracteres
            'clase' => 'required',
            'title' => 'required'
        ];

        $this->validate($request, $rules, $messages );

        $id = $request->input('id');
        $sponsor = Sponsor::find($id);
        $sponsor->name = $request->input('name');
        $sponsor->description = $request->input('description');
        $sponsor->class = $request->input('clase');
        $sponsor->website = $request->input('website');
        $sponsor->red1 = $request->input('red1');
        $sponsor->red2 = $request->input('red2');
        $sponsor->red3 = $request->input('red3');
        $sponsor->title = $request->input('title');

        $save = $sponsor->update();

          if ($request->hasFile('image')) // Verifica si la imagen viene en el request
        {
            
            //Guardar la img en nuestro proyecto
            $file = $request->file('image');
            $path = public_path() . '/images/img-sponsors';
            $filename = uniqid() . '-' . $file->getClientOriginalName();
            $moved = $file->move($path, $filename);

            // Si la imagen se movió, registra el cambio en la base de datos
            if ($moved) {
                $previousPath = $path . '/' . $sponsor->image; // Guarda la imagen anterior 
                $sponsor->image = $filename;
                $save2 = $sponsor->save(); //update
                
                if ($save2)
                    File::delete($previousPath);// Elimina la imagen anterior 
            }
            
        }

        // Validacion para determinar que tipo de mensaje enviar segun lo que se  guarde, la imagen y los datos o solo los datos. 

        if ($save && $save2)
        {
            $notification = "Se ha actualizado la imagen y los datos correctamente a el Sponsor";
            return back()->with(compact('notification'));
        }
        elseif ($save) 
        {
            $notification = "Se han actualizado los datos correctamente a el Sponsor";
            return back()->with(compact('notification'));
        }
        else 
        {
            $notification = "Hubo un problema al actualizar los datos, intente mas tarde.";
            return back()->with(compact('notification'));
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function destroy (Request $request, Sponsor $sponsor )
    {
        $id = $request->input('id');
        $sponsor = Sponsor::find($id);
        $delete = $sponsor->delete();

        if ($delete) // Verifica si se elimino 
        {
            $notification = "El sponsor ha sido eliminado satisfactoriamente.";
            return back()->with(compact('notification')); 
        }


        
    }
}
