<?php

namespace App\Http\Controllers\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{ 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function findRoles(Request $request)
    {
        $rol = new Role();
        $roles = $rol->all();
        
        return response()->json($roles);
    }
}