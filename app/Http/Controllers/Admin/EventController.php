<?php
/*15/07/2018 Lisely Fuenmayor*/
/*EventController, es el controlador para el maestro de Eventos*/

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Speaker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::paginate(10);
        return view('admin.events.index')->with(compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $speakers = Speaker::all(); // Para el crear el registro debe mostrar los expositores en el combobox.
        return view('admin.events.create')->with(compact('speakers')); // envia la consulta
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //Validaciones del backend 
        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del evento.',
            'description.required' => 'Debe agregar una descripción.',
            'dia.required' => 'Debe seleccionar un dia',
            'hora.required' => 'Debe ingresar la hora',
            'expositor.required' => 'Debe seleccionar un expositor'
            
        ];
        
        $rules = [
            
            'name' => 'required', //que sea requerido
            'description' => 'required', //que sea requerido
            'dia' => 'required',
            'hora' => 'required',
            'expositor' => 'required'
        ];

        $this->validate($request, $rules, $messages );

    
        $event = new Event();
        $event->name = $request->input('name');
        $event->description = $request->input('description');
        $event->day = date("Y/m/d", strtotime($request->input('dia')));
        $event->hour = $request->input('hora');
        $event->idspeaker = $request->input('expositor');
        $save= $event->save();

        // verifica si se guardo para dar la notificacion 
        if ($save) {
           $notification = "Se ha creado correctamente el Evento";
           return back()->with(compact('notification'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event, $id)
    {
        $events = Event::find($id);
        return view('admin.events.show')->with(compact('events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event, $id)
    {
        $speakers = Speaker::all(); // debe llamar los expositores para poder seleccionarlos en la edición. 
        $events = Event::find($id);
        return view('admin.events.edit')->with(compact('events','speakers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {

        //Validaciones del backend 
        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del evento.',
            'description.required' => 'Debe agregar una descripción.',
            'dia.required' => 'Debe seleccionar un dia',
            'hora.required' => 'Debe ingresar la hora',
            'expositor.required' => 'Debe seleccionar un expositor'
            
        ];
        
        $rules = [
            
            'name' => 'required', //que sea requerido
            'description' => 'required', //que sea requerido
            'dia' => 'required',
            'hora' => 'required',
            'expositor' => 'required'
        ];

        $this->validate($request, $rules, $messages );

        $id = $request->input('id');
        $event = Event::find($id);
        $event->name = $request->input('name');
        $event->description = $request->input('description');
        $event->day = $request->input('dia');
        $event->hour = $request->input('hora');
        $event->idspeaker = $request->input('expositor');
        $save = $event->update();

         if ($save) // Verifica si se actualizo para enviar el mensaje
        {
            $notification = "El Evento  ha sido actualizado satisfactoriamente.";
            return back()->with(compact('notification')); 
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event, Request $request)
    {
        $id = $request->input('id');
        $event = Event::find($id);
        $delete = $event->delete();

        if ($delete) // Verifica si se elimino para enviar el mensaje 
        {
            $notification = "El Evento  ha sido eliminado satisfactoriamente.";
            return back()->with(compact('notification')); 
        }
    }
}
