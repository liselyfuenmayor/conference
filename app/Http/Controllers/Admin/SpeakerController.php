<?php
/*14/07/2018 Lisely Fuenmayor*/
/*SpeakerController, es el controlador para el maestro de Expositores*/
namespace App\Http\Controllers\Admin;

use App\Speaker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class SpeakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $speakers = Speaker::paginate(10);
        return view('admin.speakers.index')->with(compact('speakers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.speakers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validaciones del backend 
        $messages = [
        'image.required' => 'Es necesario ingresar la imagen del expositor.',
        'name.required' => 'Es necesario ingresar el nombre del expositor.',
        'title.required' => 'Es necesario ingresar el nombre del expositor.',
        'description.required' => 'Es necesario ingresar la descripción.',
        'description.max' => 'La descripción admite hasta 170 caracteres como máximo.',
        'website.required' => 'Es necesario ingresar el sitio web.'/*,
            'website.active_url' => 'Debe ingresar un sitio web valido.'*/
            
        ];
        
        $rules = [
            'image' => 'required',
            'name' => 'required', //que sea requerido
            'title' => 'required', //que sea requerido
            'description' => 'required|max:170', //que sea requerido y como maximo debe tener 170 caracteres
            /*'website' => 'required|active_url'*/
            'website' => 'required'
        ];

        $this->validate($request, $rules, $messages );

        $expositor = new Speaker();

        //Guardar la img en nuestro proyecto
        $file = $request->file('image');
        $path = public_path() . '/images/img-speakers';
        $filename = uniqid() . $file->getClientOriginalName();
        $moved = $file->move($path, $filename);

        if ($moved)  // si la imagen se guardo, registra el nombre en la base de datos
        {
            $expositor->image = $filename;
        }
       

        $expositor->name = $request->input('name');
        $expositor->title = $request->input('title');
        $expositor->description = $request->input('description');
        $expositor->website = $request->input('website');
        $expositor->red1 = $request->input('red1');
        $expositor->red2 = $request->input('red2');
        $expositor->red3 = $request->input('red3');
        $save = $expositor->save();

        if ($save) // verifica si se guardo para dar la notificacion 
        {
        $notification = "Se ha creado correctamente el Expositor";
        return back()->with(compact('notification'));
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Speaker  $speaker
     * @return \Illuminate\Http\Response
     */
    public function show(Speaker $speaker, $id)
    {
        $expositor = Speaker::find($id);
        return view('admin.speakers.show')->with(compact('expositor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Speaker  $speaker
     * @return \Illuminate\Http\Response
     */
    public function edit(Speaker $speaker, $id)
    {
        $expositor = Speaker::find($id);
        return view('admin.speakers.edit')->with(compact('expositor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Speaker  $speaker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Speaker $speaker)
    {
        $save2 = false; // Inicializa la variable save2 en false, indica el evento de guardar la imagen

        //Validaciones del backend 
        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del expositor.',
            'description.required' => 'Es necesario ingresar la descripción.',
            'description.max' => 'La descripción admite hasta 170 caracteres como máximo.',
            'website.required' => 'Es necesario ingresar el sitio web'
            /*'website.active_url' => 'Debe ingresar un sitio web valido.'*/
            
        ];
        
        $rules = [
            'name' => 'required', //que sea requerido
            'description' => 'required|max:170', //que sea requerido y como maximo debe tener 170 caracteres
            'website' => 'required'
            /*'website' => 'required|active_url',*/

        ];

        $this->validate($request, $rules, $messages );

        $id = $request->input('expo');
        $expositor = Speaker::find($id);
        $expositor->name = $request->input('name');
        $expositor->title = $request->input('title');
        $expositor->description = $request->input('description');
        $expositor->website = $request->input('website');
        $expositor->red1 = $request->input('red1');
        $expositor->red2 = $request->input('red2');
        $expositor->red3 = $request->input('red3');
        $save = $expositor->update();
        /*$expositor->update($request->only('name', 'description', 'website', 'red1', 'red2', 'red3'));*/

        /*Algoritmo para guardar la imagen*/

    if ($request->hasFile('image'))/*Verifica si existe en el reques una imagen*/
        {
            
            //Guardar la img en nuestro proyecto
            $file = $request->file('image');
            $path = public_path() . '/images/img-speakers';
            $filename = uniqid() . '-' . $file->getClientOriginalName();
            $moved = $file->move($path, $filename);

            // Si la imagen se guarda en la carpe, regista el cambio en base de datos
            if ($moved) {
                $previousPath = $path . '/' . $expositor->image;
                $expositor->image = $filename;
                $save2 = $expositor->save(); //update
                
                if ($save2)  // Si se guardo, elimina la imagen anterior 
                    File::delete($previousPath);
            }
            
        }

         // Mensajes segun el tipo de guardado que se haga, si son los datos y la imagen o solo los datos

        if ($save && $save2)
        {
            $notification = "Se ha actualizado la imagen y los datos correctamente a el Expositor";
            return back()->with(compact('notification'));
        }
        elseif ($save) 
        {
            $notification = "Se han actualizado los datos correctamente a el Expositor";
            return back()->with(compact('notification'));
        }
        else 
        {
            $notification = "Hubo un problema al actualizar los datos, intente mas tarde.";
            return back()->with(compact('notification'));
        } 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Speaker  $speaker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Speaker $speaker)
    {
        $id = $request->input('expo');
        $expositor = Speaker::find($id);
        $delete = $expositor->delete();

        if ($delete) // Verifica si se elimino para dar la notificacion 
        {
            $notification = "El expositor ha sido eliminado satisfactoriamente.";
            return back()->with(compact('notification')); 
        }
    }
}
