<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
  
      $users = DB::table('users')
      ->join('roles','users.role_id','=','roles.id')
      ->select('users.*', 'roles.name as nombre_rol')
      ->get();

      $rol = new Role();
      $roles = $rol->all();
      return View::make('admin/users/index')->with('users', $users)->with('roles', $roles );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = new Role();
        $roles = $roles->all();
        return  View::make('admin/users/create')->with('roles', $roles );;
    }

    public function findUserById(Request $request)
    {
        $user = new User();
        $user = $user->find($request->id_user);
        
        return response()->json($user);
    }

    public function editUser(Request $request)
    {
        $user = new User();
        $user = $user->find($request->id_user);
        
        $user->username=$request->username;
        $user->name=$request->name;
        $user->lastname=$request->lastname;
        $user->email=$request->email;
        $user->address=$request->address;
        $user->phone=$request->phone;
        $user->role_id=$request->role_id;
        $user->save();
        return response()->json($user);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->username=$request->username;
        $user->name=$request->name;
        $user->lastname=$request->lastname;
        $user->email=$request->email;
        $user->address=$request->address;
        $user->phone=$request->phone;
        $user->password=bcrypt($request->password);
        $user->role_id=$request->role_id;
        $user->save();
        return response()->json($user);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
     {
         $user = new User();
         $user = $user->find($request->id_user);
         $user->delete();
         return response()->json($user);
     }
}
