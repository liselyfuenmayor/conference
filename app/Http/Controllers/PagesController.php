<?php

namespace App\Http\Controllers;

use App\Post;
use App\Event;
use App\Speaker;
use App\Sponsor;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PagesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        // Busqueda de post modificada, para traer solo los dos ultimos post.

        $posts = Post::orderby('created_at','DESC')->take(2)->get();
        // Busqueda de eventos 17/07/2018- se agrego ordenamiento por hora
        $events = Event::has('speaker')->orderby('hour','ASC')->get(); 
        // Busqueda de expositores 
        $speakers = Speaker::all();
        //Busqueda de sponsors 
        $sponsors = Sponsor::all();
        $sponsorsGold = Sponsor::where('class', '1')->get(); // Oro
        $sponsorsPlate = Sponsor::where('class', '2')->get();// Plata
        $sponsorsBronce = Sponsor::where('class', '3')->get(); //Bronce


        return view('welcomes', compact('posts','events', 'speakers', 'sponsors', 'sponsorsGold','sponsorsPlate','sponsorsBronce')); // Envio de los datos al landing 
    }

     public function blog() //+Nuevo método para llamar a la vista de blogs 
    {
        // Consulta esta vez todos los posts
        $posts = Post::latest('published_at')->orderby('created_at','DESC')->where('published_at', '<=', Carbon::now() )->get();
        // Envia los post a la vista 
        return view('blogs', compact('posts'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Speaker  $speaker
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, $id)
    {
        $post = Post::find($id);
        return view('showpost')->with(compact('post'));
    }

    public function sponsor(Sponsor $sponsor, $id)
    {
        $sponsor = Sponsor::find($id);
        // dd($sponsor);
        return view('oficial-sponsor')->with(compact('sponsor'));
    }
}
