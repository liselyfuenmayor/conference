<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';
    protected $primaryKey = 'id';
    protected $fillable = ['id','name','description','value'];
    public $timestamps = false;



}
