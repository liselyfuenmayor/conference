<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria


class Sponsor extends Model
{

/*Método para el borrado logico*/

    use SoftDeletes; //Implementamos 

    protected $dates = ['deleted_at']; //Registramos la nueva columna

    protected $fillable = ['name', 'content', 'title'];


    
/*Atributo cálculado ImageUrl : Sirve para cargar la  ruta completa de la imagen*/
 public function getImageUrlAttribute()
{
    $image= $this->image; 
    if($image){
            $image= '/images/img-sponsors/'.$image; 
            return $image; //url = Accesor definido en el modelo Speaker
       }

    //default
    return '/images/default.jpg';
}

/*Atributo cálculado ClassName : Sirve para obtener el nombre de la clase de sponsor*/

     public function getClassNameAttribute()
    {        
        if     ($this->class == 1 ) { return 'Oficial';    }
        elseif ($this->class == 2)  { return 'Oro';  }
        else                        { return 'Plata'; }
    }


}
