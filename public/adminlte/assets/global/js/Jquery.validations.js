/*17/07/2018 -- Lisely Fuenmayor*/
/*Archivo para el manejo de las validaciones*/

/*Esta funcion es para implementar un mensaje flash de validacion al no llenar algun campo
no esta en uso por el momento, para utilizarla coloque en el elemento input que desea validar
el siguiente evento Onblur="validateRequire("Reescriba el id del elemento");"*/

    function validateRequire(id){

/*Los formularios en diferentes vistas 
tiene los mismos id para los elementos correspondientes, se desea hacerlo mas personalizado
modifique los ids del formulario */


        $.validity.start();
        $('#'+ id).require('El campo '+ id +  ' es requerido.');
        $.validity.end();

      } 

 $(function() { 
    $('#form').validity(
        function() {
            $("#Imagen")
                .require("El campo Imagen es requerido");

            $("#Nombre")
                .require("El campo Nombre es requerido");
            $("#Descripcion")
                .require("El campo Descripcion es requerido");

            $("#Website")
                .require("El campo WebSite es requerido");

            $("#datepicker")
                .require("El campo Día es requerido");

            $("#Hora")
                .require("El campo Hora es requerido");

            $("#expositor")
                .require("El campo Expositor es requerido");

            $("#clase")
                .require("El campo Clase es requerido");

            $("#Titulo")
                .require("El campo Título es requerido");

        }
    );
});

