<?php


Route::get('/', 'PagesController@home');
Route::get('/blogs', 'PagesController@blog');
Route::get('/blogs/{id}', 'PagesController@blog')->name('post');
Route::get('/sponsor/{id}', 'PagesController@sponsor')->name('sponsor');



// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::resource('user','Admin\UsersController');
Route::get('user/', array('as' => 'indexUsuario', 'uses' => 'Admin\UsersController@index' ));
Route::get('user/create', array('as' => 'createUser', 'uses' => 'Admin\UsersController@create' ));
Route::post('user/store', array('as' => 'storeUsuario', 'uses' => 'Admin\UsersController@store'));
Route::post('user/findUserById', array('as' => 'findUserById', 'uses' => 'Admin\UsersController@findUserById'));
Route::post('user/editUser', array('as' => 'editUser', 'uses' => 'Admin\UsersController@editUser'));
Route::post('user/deleteUser', array('as' => 'deleteUser', 'uses' => 'Admin\UsersController@destroy'));



Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function(){
	Route::get('/', 'AdminController@index')->name('dashboard');

	// Posts
	Route::get('post', 'PostsController@index')->name('admin.posts.index');
	Route::get('post/create', 'PostsController@create')->name('admin.posts.create');
	Route::post('post', 'PostsController@store')->name('admin.posts.store');

	// Clients
	Route::get('client', 'ClientsController@index')->name('admin.clients.index');
	Route::get('client/create', 'ClientsController@create')->name('admin.clients.create');
	Route::post('client', 'ClientsController@store')->name('admin.clients.store');

	// Providers
	Route::get('provider', 'ProvidersController@index')->name('admin.providers.index');
	Route::get('provider/create', 'ProvidersController@create')->name('admin.providers.create');
	Route::post('provider', 'ProvidersController@store')->name('admin.providers.store');

	// users

	/*+Lisely Fuenmayor -14/07/2018 - Rutas para la gestion del landing page */

	// Speakers
	Route::get('speakers', 'SpeakerController@index')->name('admin.speakers.index');
	Route::get('speakers/create', 'SpeakerController@create')->name('admin.speakers.create');
	Route::post('speakers', 'SpeakerController@store')->name('admin.speakers.store');
	Route::get('speakers/{id}', 'SpeakerController@show')->name('admin.speakers.show');
	Route::get('speakers/edit/{id}', 'SpeakerController@edit')->name('admin.speakers.edit');
	Route::post('speakers/update', 'SpeakerController@update')->name('admin.speakers.update');
	Route::post('speakers/delete', 'SpeakerController@destroy')->name('admin.speakers.delete');

	// Sponsors
	
	Route::get('sponsors', 'SponsorController@index')->name('admin.sponsors.index');
	Route::get('sponsors/create', 'SponsorController@create')->name('admin.sponsors.create');
	Route::post('sponsors', 'SponsorController@store')->name('admin.sponsors.store');
	Route::get('sponsors/{id}', 'SponsorController@show')->name('admin.sponsors.show');
	Route::get('sponsors/edit/{id}', 'SponsorController@edit')->name('admin.sponsors.edit');
	Route::post('sponsors/update', 'SponsorController@update')->name('admin.sponsors.update');
	Route::post('sponsors/delete', 'SponsorController@destroy')->name('admin.sponsors.delete');

	// Events
	
	Route::get('events', 'EventController@index')->name('admin.events.index');
	Route::get('events/create', 'EventController@create')->name('admin.events.create');
	Route::post('events', 'EventController@store')->name('admin.events.store');
	Route::get('events/{id}', 'EventController@show')->name('admin.events.show');
	Route::get('events/edit/{id}', 'EventController@edit')->name('admin.events.edit');
	Route::post('events/update', 'EventController@update')->name('admin.events.update');
	Route::post('events/delete', 'EventController@destroy')->name('admin.events.delete');

});
